/*###########################  REPORTES  ###########################*/

function verReporte1() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=reporte&funcion=verReporte1',
        success: function (datos) {
            $(".content").html(datos);
            //listarTurnoOpe();
        }
    });
}

function verReporte2() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=reporte&funcion=verReporte2',
        success: function (datos) {
            $(".content").html(datos);
            //listarTurnoOpe();
        }
    });
}

/*###########################  HORARIOS  ########################### */

function listHorarios() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=horario&funcion=listadoHorarios',
        success: function (datos) {
            $(".content").html(datos);
            listarTurnoOpe();
        }
    });
}

function validarDatosIngHorarios() {
    var valid = true;
    var descripcion = $('#descripcion').val();
    var idturno = $('#idturno').val();
    if (descripcion == "") {
        $('#descripcionG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio (*)");
        valid = false;
    } else {
        $('#descripcionG').removeClass();
        $('#descripcionG').addClass("input-group");
        //valid = true;
    }
    if (idturno == 0) {
        $('#idturnoG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#idturnoG').removeClass();
        $('#idturnoG').addClass("input-group");
        //valid = true;
    }
    if (valid == true) {
        registrarHorarios();
    }
}

function registrarHorarios() {
    var datos = $('#formRegistroHorario').serialize();
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=horario&funcion=registrarHorario&' + datos,
        success: function (datos) {
            //var msj = datos.innerHTML;
            alert(datos);
            listHorarios();
        }
    });
}

function listarDetHorario(id, idturno) {
    var datos = "&id=" + id;//+ "&desc=" + descripcion;
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=horario&funcion=listarDetHorario' + datos,
        success: function (datos) {
            $("#sectionDetalleHorarios").html(datos);
            listarTurnoOpe(idturno);
            //$("#divListHorarios").html(datos);
        }
    });
}

function actualizarRegHorarios(idhorario, idturno) {
    //var datos = "&id=" + id + "&desc=" + descripcion;
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    var datos = $('#form_horarios_horas').serialize();
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=horario&funcion=modHorarioAddDetHorario&' + datos,
        success: function (datos) {
            //var msj = datos.innerHTML;
            alert(datos);
            listHorarios();
            listarDetHorario(idhorario, idturno);
        }
    });
}

/*###########################  FORMULARIO DE OPERADORES ###########################*/
function listadoOperadores() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listadoOperadores',
        success: function (datos) {
            $(".content").html(datos);
            listarAreasOpe();
            listarTurnoOpe();
            listarTipoUserOpe();
            listarEstadosOpe();
            listarHorarioOpe();
        }
    });
}

function listarDetOperador(id, area, turno, tipoUser, estadoUser, estMarcacionUser, idhorario) {
    var datos = "&id=" + id;
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarDetOperario' + datos,
        success: function (datos) {
            $(".content").html(datos);
            listarAreasOpe(area);
            listarTurnoOpe(turno);
            listarTipoUserOpe(tipoUser);
            listarEstadosOpe(estadoUser);
            listarEstadosMarcacionOpe(estMarcacionUser);
            listarHorarioOpe(idhorario);
            //$("#divListHorarios").html(datos);
        }
    });
}

function listarEstadosOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarEstadoOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpe_estado").html(datos);
        }
    });
}

function listarEstadosMarcacionOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarMarcacionEstadoOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpeEstado_marcacion").html(datos);
        }
    });
}

function listarAreasOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarAreaOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpe_area").html(datos);
        }
    });
}

function listarHorarioOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarHorarioOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpe_horario").html(datos);
        }
    });
}

function listarTurnoOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarTurnoOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpe_turno").html(datos);
        }
    });
}

function listarTipoUserOpe(tipo) {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=listarTipoUserOpe&tipo=' + tipo,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divOpe_tipo_user").html(datos);
        }
    });
}

function registrarOperadores() {
    var dni = $('#dni').val();
    if (dni = "") {
        alert("Ingrese el DNI");
        return false;
    }
    var datos = $('#formRegistroOperador').serialize();
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=registrarOperador&' + datos,
        success: function (datos) {
            //var msj = datos.innerHTML;
            alert(datos);
            listadoOperadores();
        }
    });
}

function actualizarOperadores() {
    var datos = $('#formActualizarOperador').serialize();
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=operador&funcion=actualizarOperador&' + datos,
        success: function (datos) {
            //var msj = datos.innerHTML;
            alert(datos);
            listMarcacion();
        }
    });
}

function validarDatos() {
    var valid = true;
    var dni = $('#dni').val();
    var nombre = $('#nombre').val();
    var apepa = $('#apepa').val();
    var apema = $('#apema').val();
    var idarea = $('#idarea').val();
    var idturno = $('#idturno').val();
    var idtipo_usuario = $('#idtipo_usuario').val();
    if (dni == "") {
        $('#dniG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio (*)");
        valid = false;
    } else {
        $('#dniG').removeClass();
        $('#dniG').addClass("input-group");
        //valid = true;
    }
    if (nombre == "") {
        $('#nombreG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#nombreG').removeClass();
        $('#nombreG').addClass("input-group");
        //valid = true;
    }
    if (apepa == "") {
        $('#apepaG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#apepaG').removeClass();
        $('#apepaG').addClass("input-group");
        //valid = true;
    }
    if (apema == "") {
        $('#apemaG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#apemaG').removeClass();
        $('#apemaG').addClass("input-group");
        //valid = true;
    }
    if (idarea == 0) {
        $('#idareaG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#idareaG').removeClass();
        $('#idareaG').addClass("input-group");
        //valid = true;
    }
    if (idturno == 0) {
        $('#idturnoG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#idturnoG').removeClass();
        $('#idturnoG').addClass("input-group");
        //valid = true;
    }
    if (idtipo_usuario == 0) {
        $('#idtipo_usuarioG').addClass("input-group errorImput1");
        $('#messageError').html("Este Dato es Obligatorio(*)");
        valid = false;
    } else {
        $('#idtipo_usuarioG').removeClass();
        $('#idtipo_usuarioG').addClass("input-group");
        //valid = true;
    }
    if (valid == true) {
        registrarOperadores();
    }
}

/*###########################  FORMULARIO PRINCIPAL DE MARCACION ###########################*/

function listHorasTrabajadasxMes() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=mostrarHorasTrabajadasxMes',
        success: function (datos) {
            $(".content").html(datos);
            //$("#divListHorarios").html(datos);
        }
    });
}

function listHorasTrabajadasxDia() {
    if (typeof relojito != 'undefined')
        clearTimeout(relojito);
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=mostrarHorasTrabajadasxDia',
        success: function (datos) {
            $(".content").html(datos);
            //$("#divListHorarios").html(datos);
        }
    });
}

function listMarcacion() {
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=listMarcacion',
        success: function (datos) {
            $(".content").html(datos);
            //$("#divListHorarios").html(datos);
        }
    });
}

function asistenciaPersonalById(id) {
    var datos = "&id=" + id;
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=monstrarRegistrosAsisById' + datos,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divMostrarResultados").html(datos);
        }
    });
}

function listardetHorario(id) {
    var datos = "&id=" + id;
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=listardetHorario' + datos,
        success: function (datos) {
            //$(".content").html(datos);
            $("#divListHorarios").html(datos);
        }
    });
}

function marcar(idoperador1, tipo1, idturno) {
    var hora1 = $("#hora_marcacion").val(); //hora actual
    var ipVisitante = $("#ipVisitante").val();
    var datos = "&idoperador=" + idoperador1 + "&tipo=" + tipo1 + "&hora=" + hora1 + "&idturno=" + idturno + "&ipVisitante=" + ipVisitante;
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=marcacion' + datos,
        success: function (datos) {
            //$(".content").html(datos);
            //$("#divMostrarResultados").html(datos);
            location.href = "index.php";
        }
    });
}

function actualizarMarcacion() {
    var datos = $('#formModificarMarcacion').serialize();
    //var datos = "&idoperador=" + idoperador1 + "&tipo=" + tipo1 + "&hora=" + hora1 + "&idturno=" + idturno;
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=actualizarMarcacion&' + datos,
        success: function (datos) {
            alert(datos);
            //$('#modalCambiarEstado').modal('hide');            
            //$(".content").html(datos);
            //$("#divMostrarResultados").html(datos);
            //location.href = "index.php";
            //listarDetOperador(id, area, turno, tipoUser, estadoUser);
            $('#formModificarMarcacion').submit();
            //index();
            //listMarcacion();
            //asistenciaPersonalById(85);
            //listMarcacion();
        }
    });
}

function marcacion(boton, idoperador, tipo, idturno) {
    var hora1 = $("#hora_marcacion").val(); //hora actual
    var h_ingreso = $("#h_ingreso").val();
    var h_break = $("#h_break").val();
    var h_end_break = $("#h_end_break").val();
    var h_salida = $("#h_end_day").val();
    //alert(tipo);
    if (tipo == '1') {
//        if ((horasSegundos(hora1) <= horasSegundos(h_ingreso) - 15400)) {
//            alert("Aún no es la hora de ingreso.");
//        } else {
        boton.disabled = true;
        boton.value = 'Marcando..';
        marcar(idoperador, tipo, idturno);
//        }
    } else if (tipo == '2') {
        if ((horasSegundos(hora1) <= horasSegundos(h_break) - 15400)) {
            alert("Aún no es la hora de almuerzo.");
        } else {
            boton.disabled = true;
            boton.value = 'Marcando..';
            marcar(idoperador, tipo, idturno);
        }
    } else if (tipo == '3') {
        if (horasSegundos(hora1) <= horasSegundos(h_end_break) - 13600) {
            alert("Aún no culmina su break.");
        } else {
            boton.disabled = true;
            boton.value = 'Marcando..';
            marcar(idoperador, tipo, idturno);
        }
    } else if (tipo == '4') {
        if (horasSegundos(hora1) <= horasSegundos(h_salida) - 119000) {
            alert("Aún no culmina su turno.");
        } else {
            boton.disabled = true;
            boton.value = 'Marcando..';
            marcar(idoperador, tipo, idturno);
        }
    } else {
        marcar(idoperador, tipo, idturno);
    }
}

function horasSegundos(hora) {
    var arHora1 = hora.split(":");
    var hh1 = parseInt(arHora1[0], 10);
    var mm1 = parseInt(arHora1[1], 10);
    var ss1 = parseInt(arHora1[2], 10);
    var seg1 = (hh1 * 3600) + mm1 * 60 + ss1;
    return seg1;
}

function deshabilitar(id, descripcion, attributo, tabla) {
    if (confirm('Esta apunto de deshabilitar a ' + descripcion + ' de la tabla ' + tabla + '\n¿Desea Continuar?'))
    {
        var segundos = 1;
        var datos = '&attributo=' + attributo + '&id=' + id + '&tabla=' + tabla;
        $.ajax({
            url: 'index.php',
            type: 'POST',
            async: true,
            data: 'controlador=principal&funcion=deshabilitar' + datos,
            success: function (datos) {
                var timeSlide = 1000 * segundos;
                setTimeout(function () {
                    listadoOperadores();
                    alert("Se deshabilitó correctamente " + descripcion);
                }, (timeSlide + 500));
            }
        });
    } else
    {
        alert('Se canceló ésta Operacion.');
    }
}

function habilitar(id, descripcion, attributo, tabla) {
    if (confirm('Esta apunto de habilitar a ' + descripcion + ' de la tabla ' + tabla + '\n¿Desea Continuar?'))
    {
        var segundos = 1;
        var datos = '&attributo=' + attributo + '&id=' + id + '&tabla=' + tabla;
        $.ajax({
            url: 'index.php',
            type: 'POST',
            async: true,
            data: 'controlador=principal&funcion=habilitar' + datos,
            success: function (datos) {
                var timeSlide = 1000 * segundos;
                setTimeout(function () {
                    listadoOperadores();
                    alert("Se habilitó correctamente " + descripcion);
                }, (timeSlide + 500));
            }
        });
    } else
    {
        alert('Se canceló ésta Operacion.');
    }
}

function restablecerMarcacion(tipoTurno) {
    var datos = "&tipoTurno=" + tipoTurno;
    $.ajax({
        url: 'index.php',
        type: 'POST',
        async: false,
        data: 'controlador=principal&funcion=restablecereMarcacion&' + datos,
        success: function (datos) {
            alert(datos);
            //$(".content").html(datos);
            //$("#divMostrarResultados").html(datos); 
            $('#formModificarMarcacion').submit();
        }
    });
}

/*###########################       FORMULARIO LOGIN                ###########################*/
function salir(urlBase) {
    $.ajax({
        url: 'index.php',
        type: 'GET',
        async: true,
        data: 'controlador=login&funcion=salir',
        success: function (datos) {
            //location.reload();
            location.href = urlBase + "php/views/";
        },
        error: {}
        //error: muestraError
    });
}

function index(urlBase) {
    $.ajax({
        url: 'index.php',
        type: 'GET',
        async: true,
        data: 'controlador=principal&funcion=index',
        success: function (datos) {
            //location.reload();
            //location.href = urlBase + "php/views/";
            listMarcacion();
        }
        //error: muestraError
    });
}
