<?php

include 'Config.php';

class CConexion {

    //private static $CONFIG = null;
    private $host, //direccion del host
            $usuario, //usuario de la base de datos
            $password, //contraseña del usuario
            $nombreBD, //nombre de la base de datos a conectar
            $datos,
            $tabla,
            $Sql;
    protected $varConexion; //variable de conexion 

    public function CConectarse() {
        $config = new Config();
        $this->host = $config->getConfig()['host'];
        $this->usuario = $config->getConfig()['user'];
        $this->password = $config->getConfig()['clave'];
        $this->nombreBD = $config->getConfig()['baseDatos'];
        $this->crearConexion();
    }

    private function crearConexion() {//crea la conexion al base de datos con las variables creadas
        try {
            $conex = $this->varConexion = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->nombreBD . "", $this->usuario, $this->password);
            $conex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($conex == false) {
                throw new Exception('No Se Pudo Realizar La Conexión');
            }
        } catch (Exception $e) {
            $mensajeError = $e->getMessage();
            echo '<h1>&nbsp;</h1><h1 style="color: #2A3F54;text-align: center">Han reiniciado el Router y se a cambiado la IP PUBLICA, Esto Genera un error al intentar conectarse a la base de datos desde el servidor local SERVER-DI</h1>
<h2 style="color: #1479B8;text-align: center">Contactarse con 940151636 &oacute; 945019336 para informar sobre esta ocurrencia</h2>
<h2 style="color: #1479B8;text-align: center">Para Marcar su Asistencia en el servidor externo de respaldo de click </h2>
<h2 style="text-align: center"><b style="text-align: center ;font-size: 45px;border-radius: 15px;background-color: yellow"><a style="margin: 5px;padding: 5px;font-size: 45px;color: red;" href="http://dataimage-pro.com/sisasistenciadi/php/views/">&nbsp;Aqui&nbsp;&nbsp;</a></b></h2>';
            if ($_POST['login_nroDocumento'] == '46241427') {
                echo $mensajeError;
            }else {
                error_reporting("E-PARSE");
            }
        }
        return $conex;
    }

    public function sqlInsert($pDatos, $pTabla) {
        $this->datos = $pDatos;
        $this->tabla = $pTabla;
        $this->Sql = 'INSERT INTO ' . $this->tabla . ' (';
        $totalIndices = count($this->datos);
        $y = 0;
        foreach ($this->datos as $indice => $contenido) {
            $this->Sql .= $indice;
            $y++;
            if ($y == $totalIndices) {
                $this->Sql .= ') ';
            } else {
                $this->Sql .= ',';
            }
        }
        $this->Sql .= 'VALUES (';
        $totalIndices = count($this->datos);
        $y = 0;
        foreach ($this->datos as $indice => $contenido) {
            if (is_numeric($contenido)) {
                $this->Sql .= "'$contenido'";
            } else {
                $this->Sql .= "'$contenido'";
            }
            $y++;
            if ($y == $totalIndices) {
                $this->Sql .= ')';
            } else {
                $this->Sql .= ',';
            }
        }
        $query = $this->Sql;
        try {
            $reg = $this->varConexion->prepare($query);
            $reg->execute();
            $lastID = $this->varConexion->lastInsertId();
            $msj = array(
                "status" => "ok",
                "message" => "Se ejecutó correctamente la consulta!.. El último dato de $pTabla ingresado fue: $lastID ",
                "lastID" => $lastID,
                "query" => $query);
        } catch (Exception $e) {
            $msj = array(
                "status" => "error",
                "message" => "Hubo un problema al intentar insertar los datos, Error: " . $e->getMessage(),
                "lastID" => null,
                "query" => $query
            );
        }
        return $msj;
    }

    public function sqlUpdate($pDatos, $pTabla) {
        $this->datos = $pDatos;
        $this->tabla = $pTabla;
        $this->Sql = 'UPDATE ' . $this->tabla . ' SET ';
        $totalIndices = count($this->datos);
        $y = 0;
        foreach ($this->datos as $indice => $contenido) {
            if (is_numeric($contenido)) {
                $this->Sql .= $indice . "='$contenido'";
            } else {
                $this->Sql .= $indice . "='$contenido'";
            }
            $y++;
            if ($y == $totalIndices) {
                $this->Sql .= ';';
            } else if ($y == $totalIndices - 1) {
                $this->Sql .= ' WHERE ';
            } else {
                $this->Sql .= ',';
            }
        }
        $query = $this->Sql;
        try {
            $reg = $this->varConexion->prepare($query);
            $reg->execute();
            //$lastID = $this->varConexion->lastUpdatedId();
            $msj = array(
                "status" => "ok",
                "message" => "Se ejecutó correctamente la consulta!.. El registro de $pTabla fué modificado ",
                "query" => $query
            );
        } catch (Exception $e) {
            $msj = array(
                "status" => "error",
                "message" => "Hubo un problema al intentar actualizar, Error: " . $e->getMessage(),
                "query" => $query
            );
        }
        return $msj;
    }

    public function sqlDelete($id, $pTabla, $idAttri) {
        try {
            $query = "update $pTabla set idstate=2 where $idAttri = ?";
            $dbh = $this->varConexion->prepare($query);
            $dbh->bindParam(1, $id);
            $dbh->execute();
            $msj = array(
                "status" => "ok",
                "message" => "Se ejecutó correctamente la consulta!.. El registro de $pTabla fué deshabilitado ",
                "query" => $query
            );
        } catch (Exception $e) {
            $msj = array(
                "status" => "error",
                "message" => "Hubo un problema al intentar eliminar, Error: " . $e->getMessage(),
                "query" => $query
            );
        }
        return $msj;
    }

    public function insUpdDel($query) {
        try {
            $dbh = $this->varConexion->prepare($query);
            $dbh->execute();
            $msj = array(
                "status" => "ok",
                "message" => "Se ejecutó correctamente la consulta!..",
                "query" => $query
            );
        } catch (Exception $e) {
            $msj = array(
                "status" => "error",
                "message" => "Hubo un error al ejecutar la consulta. Error: " . $e->getMessage(),
                "query" => $query
            );
        }
        return $msj;
    }

    public function consultasLibres($query) {
        try {
            $dbh = $this->varConexion->prepare($query);
            $dbh->execute();
            $reg = $dbh->fetchAll(PDO::FETCH_ASSOC);
            $cant_regs = count($reg);
            if ($cant_regs > 0) {
                $msj = array(
                    "status" => "ok",
                    "messaje" => "Se ejecutó correctamente la consulta!..",
                    "object" => $reg,
                    "query" => $query
                );
            } else {
                $msj = array(
                    "status" => "null",
                    "messaje" => "No se encontraron registros en la consulta!..",
                    "object" => $reg,
                    "query" => $query
                );
            }
        } catch (Exception $e) {
            $msj = array(
                "status" => "error",
                "messaje" => "Hubo un error al ejecutar la consulta. Error: " . $e->getMessage(),
                "object" => null,
                "query" => $query
            );
        }
        return $msj;
    }

    public function consultasXnParam($query, $datos) {
        $dbh = $this->varConexion->prepare($query);
        $cant = count($datos);
        for ($i = 1; $i <= $cant; $i++) {
            $dbh->bindParam($i, $datos[$i - 1]);
        }
        $dbh->execute();
        $reg = $dbh->fetchAll(PDO::FETCH_ASSOC);
        //$dat = array("objet"=>$reg,"query"=>$datos);
        return $reg;
    }

    public function cerrarConexion() {
        $this->varConexion = null;
    }

}

//$cn = new CConexion();
//$query1 = "select * from operador";
//$cn->CConectarse();
//$res = $cn->consultasLibres($query1);
//print_r($res);
