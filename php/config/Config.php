﻿<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author alucard
 */
class Config {
    /* public static $CONFIG = array(
      'host' => 'localhost', //localhost
      'user' => 'root', //c
      'clave' => 'zerokernel', //zerokernel
      'baseDatos' => 'dataimag_dbcontrol', //dataimag_dbcontrol
      'urlBase' => "http://server-di:8080/sisasistenciadi/",
      'nombreSis' => "Sis. Asistencia",
      'empresa' => "Dataimage Pro SAC",
      'webEmpresa' => 'http://dataimage-pro.com/',
      'anio' => "2017",
      ); */

    public function getConfig() {
        $config = array(
            'host' => 'dataimage-pro.com', //localhost
            'user' => 'dataimag_admin', //root
            'clave' => '@dmin2016', //zerokernel
            'baseDatos' => 'dataimag_dbcontrol1', //dataimag_dbcontrol
            'urlBase' => "http://server-di:8080/sisasistenciadi/", //http://digitalworkingdelperu.com
            'nombreSis' => "Sis. Asistencia",
            'empresa' => "Dataimage Pro SAC",
            'webEmpresa' => 'http://dataimage-pro.com/',
            'anio' => "2017",
            'nomDias' => array('0' => "Domingo",'1' => "Lunes", '2' => "Martes", '3' => "Miércoles", '4' => "Jueves", '5' => "Viernes", '6' => "Sábado", '7' => "Domingo",),
            'nomMeses' => array('01' => "Enero", '02' => "Febrero", '03' => "Marzo", '04' => "Abril", '05' => "Mayo", '06' => "Junio", '07' => "Julio", '08' => "Agosto", '09' => "Septiembre", '10' => "Octubre", '11' => "Nobiembre", '12' => "Diciembre",),
        );
        return $config;
    }

    /* static $CONFIG = array(
      'host' => 'dataimage-pro.com', //localhost
      'user' => 'dataimag_admin', //root
      'clave' => '#S0p0rt3198#', //zerokernel
      'baseDatos' => 'dataimag_dbcontrol1', //dataimag_dbcontrol
      'urlBase' => "http://localhost/sisasistenciadi/",
      'nombreSis' => "Sis. Asistencia",
      'empresa' => "Dataimage Pro SAC",
      'webEmpresa' => 'http://dataimage-pro.com/',
      'anio' => "2017",
      'nomDias' => array('1' => "Lunes", '2' => "Martes", '3' => "Miércoles", '4' => "Jueves", '5' => "Viernes", '6' => "Sábado", '7' => "Domingo",),
      'nomMeses' => array('01' => "Enero", '02' => "Febrero", '03' => "Marzo", '04' => "Abril", '05' => "Mayo", '6' => "Junio", '07' => "Julio", '08' => "Agosto", '09' => "Septiembre", '10' => "Octubre", '11' => "Nobiembre", '12' => "Diciembre",),
      );
     */

    public function getAnio() {
        $anio = date("Y");
        return $anio;
    }

    public function getHoraActual() {
        $hora = date("H:i:s");
        $nuevafecha = strtotime('-1 hour', strtotime($hora));
        //$nuevafecha = strtotime('+13 minute', strtotime($fecha));
        //$nuevafecha = strtotime('+30 second', strtotime($fecha));
        $nuevafecha = date("H:i:s", $nuevafecha);
        //return $nuevafecha;
        return $hora;
    }

    public function getFechaCortaActual() {
        $fecha = date("Y-m-d");
        return $fecha;
    }

    public function getFechaLargaActual() {
        $fecha = date("Y-m-d H:i:s");
        return $fecha;
    }

    public function getFechaFormat($formato) {
        $fecha = date($formato);
        return $fecha;
    }

    public function getNombreDiaSemana() {
        $dia = date("w");
        $array1 = $this->getConfig();
        $arrayDias = $array1['nomDias'];
        return $arrayDias[$dia];
    }

    public function getNombreMes() {
        $mesActual = date("m");
        $array1 = $this->getConfig();
        $arrayMes = $array1['nomMeses'];
        return $arrayMes[$mesActual];
    }

    public function getNombreMesBy($mesActual) { 
        $array1 = $this->getConfig();
        $arrayMes = $array1['nomMeses'];
        return $arrayMes[$mesActual];
    }
    
    public function getSegundoHoras($segun) {
        if ($segun < 0) {
            $segundos = $segun * (-1);
        } else {
            $segundos = $segun;
        }
        $var1 = $segundos / 3600;
        if (strpos($var1, ".")) {
            $hora = substr($var1, 0, strpos($var1, ".") - strlen($var1));
        } else {
            $hora = $var1;
        }
        $var2 = ($segundos - ($hora * 3600) ) / 60;
        if (strpos($var2, ".")) {
            $min = substr($var2, 0, strpos($var2, ".") - strlen($var2));
        } else {
            $min = $var2;
        }
        $seg = $segundos - ($hora * 3600) - ($min * 60);
        if ($hora < 10) {
            $horaf = "0$hora";
        } else {
            $horaf = $hora;
        }
        if ($min < 10) {
            $minf = "0$min";
        } else {
            $minf = $min;
        }
        if ($seg < 10) {
            $segf = "0$seg";
        } else {
            $segf = $seg;
        }
        return "$horaf:$minf:$segf";
    }

    public function getHorasSegundo($hora) {
        $arHora = explode(":", $hora);
        $hh1 = $arHora[0];
        $mm1 = $arHora[1];
        $ss1 = $arHora[2];
        $segundos = $hh1 * 3600 + $mm1 * 60 + $ss1;
        return $segundos;
    }

    function get_nombre_dia($fecha) {
        $fechats = strtotime($fecha);
        $dia = date('w', $fechats);
        $nombre = $this->getConfig()['nomDias'][$dia];
        return $nombre;
    }
}
