<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of operadorController
 *
 * @author alucard
 */
class horarioController {

    public function listadoHorarios() {
        require '../models/MHorario.php';
        $oHorario = new MHorario();
        $objeto = $oHorario->mostrarHorarios();
        $arrayHorario = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/tables/listadoHorarios.php';
    }

    public function listarDetHorario() {
        $id = $_POST['id'];
        require '../models/MHorario.php';
        $oHorario = new MHorario();
        $objeto = $oHorario->mostrarDetHorarios($id);
        $arrayHorario = $objeto['object'];
        for ($x = 0; $x < count($arrayHorario); $x++) {
            // $aDia[$x] = $arrayHorario[$x]['dia'];
        }
        //print_r($arrayHorarios);
        require '../views/tables/listDetHorarios.php';
    }

    public function registrarHorario() {
        $datos = $_POST;
        //echo print_r($datos);
        //exit();
        require '../models/MHorario.php';
        $oHorario = new MHorario();
        $objeto = $oHorario->registrarHorario($datos);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

    public function modHorarioAddDetHorario() {
        foreach ($_REQUEST as $indice => $contenido) {
            if ($indice != "PHPSESSID") {
                if (substr($indice, 0, 6) == "numDia") {
                    $aNumDia[] = $contenido;
                } else if (substr($indice, 0, 7) == "ingreso") {
                    $aIngreso[] = $contenido;
                } else if (substr($indice, 0, 5) == "break") {
                    $abreak[] = $contenido;
                } else if (substr($indice, 0, 7) == "bkbreak") {
                    $abkbreak[] = $contenido;
                } else if (substr($indice, 0, 6) == "salida") {
                    $asalida[] = $contenido;
                } else {
                    $datos[$indice] = $contenido;
                }
            }
        }
         /*echo print_r($aNumDia) . "<br>";
          echo print_r($aIngreso) . "<br>";
          echo print_r($abreak) . "<br>";
          echo print_r($abkbreak) . "<br>";
          echo print_r($asalida) . "<br>";
          exit(); */
        require '../models/MHorario.php';
        $oHorario = new MHorario();
        $objeto = $oHorario->modHorarioAddDetHorario($datos, $aNumDia, $aIngreso, $abreak, $abkbreak, $asalida);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

}
