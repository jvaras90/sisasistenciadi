<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginController
 *
 * @author alucard
 */
class loginController {

    public function index() {
        if (isset($_POST['login_nroDocumento'])) {
            require '../models/Mlogin.php';
            $o_login = new Mlogin();
            $dniUser = $_POST['login_nroDocumento'];
            $login = $o_login->validarUsuario($dniUser);
            if ($login['status'] == "ok") {
                $data = $login['object'];
                $_SESSION['id_operador'] = $data[0]['id_operador'];
                $_SESSION['nombres'] = utf8_decode($data[0]['nombre']) . " " . utf8_decode($data[0]['apepa']) . " " . utf8_decode($data[0]['apema']);
                $_SESSION['fec_ingreso'] = $data[0]['f_ing'];
                $_SESSION['fec_egreso'] = $data[0]['f_egr'];
                $_SESSION['dni_user'] = $data[0]['dni'];
                $_SESSION['foto'] = $data[0]['foto'];
                $_SESSION['idturno'] = $data[0]['idturno'];
                $_SESSION['turno'] = $data[0]['turno'];
                $_SESSION['idarea'] = $data[0]['idarea'];
                $_SESSION['area'] = $data[0]['area'];
                $_SESSION['id_nivel'] = $data[0]['idtipo_usuario'];
                $_SESSION['nivel'] = $data[0]['tipo_user'];
                $_SESSION['idmarcacion_estado'] = $data[0]['idmarcacion_est'];
                $_SESSION['idestado'] = $data[0]['idestado'];
                $_SESSION['idsede'] = $data[0]['idsede'];
                $_SESSION['sede'] = $data[0]['sede'];
                $_SESSION['idhorario'] = $data[0]['idhorario'];
                $_SESSION['horario'] = $data[0]['horario'];

                $userAgent = $_SERVER[HTTP_USER_AGENT];
                $userAgent1 = strtolower($userAgent);
                if (strpos($userAgent1, "android") !== false || strpos($userAgent1, "iphone") !== false) {
                    $_SESSION['mobil'] = 1;
                } else {
                    $_SESSION['mobil'] = 0;
                }
                //$_SESSION[''] = $data[0][''];
                //para abrir la vista destino 
                header('Location: index.php');
                //require '../views/principal.php';
            } else {
                session_destroy();
                $mensaje = "Usuario no registrado o bloqueado";
                require '../views/logueo.php';
            }
        } else {
            $mensaje = '';
            require '../views/logueo.php';
        }
        //var_dump($login);
    }

    public function salir() {
        session_destroy();
    }

}
