<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of operadorController
 *
 * @author alucard
 */
class operadorController {

    public function listadoOperadores() {
        require '../models/MOperador.php';
        $oOperador = new MOperador();
        $objeto = $oOperador->listarOperadores();
        $arrayOperador = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/tables/listadoOperadores.php';
    }

    public function listarDetOperario() {
        $id = $_POST['id'];
        require '../models/MOperador.php';
        $oOperador = new MOperador();
        $objeto = $oOperador->detalleOperador($id);
        $arrayOperador = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/detOperador.php';
    }

    public function listarAreaOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarAreas();
        $arrayAreaOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listaArea.php';
    }

    public function listarHorarioOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarHorarios();
        $arrayHorarioOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listaHorarios.php';
    }
    
    public function listarTurnoOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarTurnos();
        $arrayTurnoOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listaTurno.php';
    }

    public function listarTipoUserOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarTipoUserOpe();
        $arrayTipoUserOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listarTipoUserOpe.php';
    }

    public function listarEstadoOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarEstados();
        $arrayEstadoOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listaEstado.php';
    }

    public function listarMarcacionEstadoOpe() {
        $tipo = $_POST['tipo'];
        require '../models/MDropDownList.php';
        $oArea = new MDropDownList();
        $objeto = $oArea->listarEstadosMarcados();
        $arrayEstadoOpe = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/dropDownList/listaEstadoMarcacion.php';
    }

    public function registrarOperador() {
        $datos = $_POST;
        //echo print_r($datos);
        //exit();
        require '../models/MOperador.php';
        $oOperador = new MOperador();
        $objeto = $oOperador->registrarOperador($datos);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

    public function actualizarOperador() {
        $datos = $_POST;
        //echo print_r($datos);
        //exit();
        require '../models/MOperador.php';
        $oOperador = new MOperador();
        $objeto = $oOperador->actualizarOperador($datos);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

    public function modificarFoto() {
        $datos = $_POST;
        //var_dump($datos);
        //exit();
        require '../models/MOperador.php';
        $oOperador = new MOperador();
        $objeto = $oOperador->modificarFoto($datos);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

}
