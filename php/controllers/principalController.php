<?php

//session_start();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of principalControllers
 *
 * @author alucard
 */
//include_once '../config/Config.php';
class principalController {

    public function index() {
        require '../views/principal.php';
    }

    public function listMarcacion() {
        require '../views/marcacion.php';
    }

    public function listardetHorario() {
        $id = $_POST['id'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->listarHorarios($id);
        $arrayHorarios = $objeto['object'];
        //print_r($arrayHorarios);
        require '../views/vistaHorarios.php';
    }

    public function marcacion() {
        $tipo = $_POST['tipo'];
        $hora = $_POST['hora'];
        $idoperador = $_POST['idoperador'];
        $idturno = $_POST['idturno'];
        $ipVisitante = $_POST['ipVisitante'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->registrarMarcacion($idoperador, $tipo, $hora, $idturno,$ipVisitante);
        print_r($objeto);
    }

    public function actualizarMarcacion() {
        $datos = $_POST;
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->actualizarMarcacion($datos);
        //print_r($objeto);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

    public function monstrarRegistrosAsisById() {
        $id = $_POST['id'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->mostrarRegAsistenciaByID($id,1,'','');
        $arrayRegistros = $objeto['object'];
        require '../views/tables/listRegistroAsistencia.php';
    }

    public function mostrarHorasTrabajadasxMes() {
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->mostrarHorasTrabajadasxMes();
        $arrayRegistros = $objeto['object'];
        require '../views/tables/listAsistenciaxMes.php';
    }

    public function mostrarHorasTrabajadasxDia() {
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->mostrarHorasTrabajadasxDia();
        $arrayRegistros = $objeto['object'];
        require '../views/tables/listAsistenciaxDia.php';
    }

    public function mostrarDetalleMarcacion() {
        $id = $_POST['id'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->mostrarDetalleMarcacion($id);
        $arrayRegistros1 = $objeto['object'];
        require '../views/modal/modalActMarcacion.php';
    }

    public function restablecereMarcacion() {
        $tipoTurno = $_POST['tipoTurno'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->restablecereMarcacion($tipoTurno);
        $msj = $objeto['message'];
        require '../views/showMessages.php';
    }

    public function deshabilitar() {
        $id = $_POST['id'];
        $attributo = $_POST['attributo'];
        $tabla = $_POST['tabla'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->quitar($tabla, $attributo, $id);
        //print_r($objeto);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

    public function habilitar() {
        $id = $_POST['id'];
        $attributo = $_POST['attributo'];
        $tabla = $_POST['tabla'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->habilitar($tabla, $attributo, $id);
        //print_r($objeto);
        $msj = $objeto['message'];
        //return $msj;
        require '../views/showMessages.php';
    }

}
