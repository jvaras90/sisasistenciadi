<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reporteController
 *
 * @author Hvaras
 */
class reporteController {

    //put your code here
    public function verReporte1() {
        require '../views/reporte2.php';
    }

    public function verReporte2() {
        require '../views/reporte3.php';
    }

    public function mostrarAsistenciaPorRangoFec(){
        $id = $_POST['idoperador'];
        $fecIni = $_POST['fecDesde'];
        $fecFin = $_POST['fecHasta'];
        require '../models/MHorario.php';
        $oHorarios = new MHorario();
        $objeto = $oHorarios->mostrarRegAsistenciaByID($id,2,$fecIni,$fecFin);
        $arrayRegistros = $objeto['object'];
        require '../views/tables/listRegistroAsistencia.php';
    }
}
