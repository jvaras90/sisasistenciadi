<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MDropDownList
 *
 * @author alucard
 */
include'../config/CConexion.php';
//session_start();

class MDropDownList extends CConexion {

    public function __construct() {
        parent::CConectarse();
    }

    public function listarEstados() {
        //if ($_SESSION['id_nivel'] == 1) {
        //  $cond = " 1=1 ";
        //} else {
        $cond = " idestado in (1,2) ";
        //}
        $query = "select * from estado where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function listarEstadosMarcados() {
        $cond = " idestado in (3,4,5,6,7) ";
        $query = "select * from estado where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function listarTurnos() {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1=1 ";
        } else {
            $cond = " idestado = 1 ";
        }
        $query = "select * from turno where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function listarAreas() {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1=1 ";
        } else {
            $cond = " idestado = 1 ";
        }
        $query = "select * from area where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function listarHorarios() {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1=1 ";
        } else {
            $cond = " idestado = 1 ";
        }
        $query = "select * from horario where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }
    
    public function listarTipoUserOpe() {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1=1 ";
        } else {
            $cond = " idestado = 1 ";
        }
        $query = "select * from tipo_usuario where $cond";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

}
