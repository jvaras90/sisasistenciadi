<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MHorario
 *
 * @author alucard
 */
include '../config/CConexion.php';

//session_start();

class MHorario extends CConexion {

    public function __construct() {
        parent::CConectarse();
    }

    public function listarHorarios($id) {
        $numDiaActual = date("w");
        $query = "select * from horario_marc where idhorario = $id and dia = $numDiaActual";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function mostrarHorarios() {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1 = 1 ";
        } else {
            $cond = " idestado = 1 ";
        }
        $query = "select * from horario where $cond order by idturno";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function mostrarDetHorarios($id) {
        if ($_SESSION['id_nivel'] == 1) {
            $cond = " 1 = 1 ";
        } else {
            $cond = " hm.idestado = 1 ";
        }
        $query = "select * from horario_marc hm inner join horario h on h.idhorario = hm.idhorario
            where hm.idhorario = $id and $cond order by dia";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function registrarHorario($dat) {
        $datos['descripcion'] = utf8_encode($dat['descripcion']);
        $datos['idturno'] = $dat['idturno'];
        $cn = parent::sqlInsert($datos, 'horario');
        return $cn;
    }

    public function modHorarioAddDetHorario($dat, $aNumDia, $aIngreso, $abreak, $abkbreak, $asalida) {
        //print_r($dat);
        //exit();
        $idhorario = $dat['idhorario'];
        /* ------------------- actualizar el horario ------------------- */
        $datos['descripcion'] = utf8_encode($dat['descripcion']);
        $datos['idturno'] = $dat['idturno'];
        $datos['idhorario'] = $idhorario;
        $cn = parent::sqlUpdate($datos, 'horario');
        /* ------------------- eliminar todos los elementos de horario_marc ------------------- */
        $query1 = "delete from horario_marc where idhorario= $idhorario";
        parent::insUpdDel($query1);
        /* ------------------- agregar nuevos elementos al horario_marc ------------------- */
        for ($x = 0; $x < count($aNumDia); $x++) {
            $query = "INSERT INTO horario_marc (dia, h_ingreso, h_break, h_backbreak, h_salida, idhorario, idestado) VALUES 
            ('" . $aNumDia[$x] . "', '" . $aIngreso[$x] . "', '" . $abreak[$x] . "','" . $abkbreak[$x] . "', '" . $asalida[$x] . "', '$idhorario', '1')";
            parent::insUpdDel($query);
        }
        return $cn;
    }

    public function getIdMarcacion() {
        $conf = new Config();
        $fecha_hoy = $conf->getFechaCortaActual();
        $query = "select m.idmarcacion, m.idoperador, m.fecha_marcada ,  m.begin_day , m.break , m.backbreak , m.end_day ,
                hm.h_ingreso,hm.h_break,hm.h_backbreak,hm.h_salida, m.idturno,t.descripcion turno, ip_marc_ing,ip_marc_bre,ip_marc_bbk,ip_marc_end 
                from marcacion m 
                inner join  turno t on m.idturno = t.idturno
		inner join horario ho on ho.idhorario = m.idturno
		inner join horario_marc hm on ho.idhorario = hm.idhorario
                where idoperador = " . $_SESSION['id_operador'] . " and fecha_marcada='$fecha_hoy ' order by fecha_marcada desc limit 1";
//select * from marcacion where idoperador = " . $_SESSION['id_operador'] . " and fecha_marcada='$hoy' order by fecha_marcada desc limit 1";
        $obj = parent::consultasLibres($query);
        $datos = $obj['object'];
        $idMarcacion = $datos[0]['idmarcacion'];
        return $datos;
    }

    public function getRealIP() {
        if ($_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
            $client_ip = (!empty($_SERVER['REMOTE_ADDR']) ) ?
                    $_SERVER['REMOTE_ADDR'] :
                    ( (!empty($_ENV['REMOTE_ADDR']) ) ?
                            $_ENV['REMOTE_ADDR'] :
                            "unknown" );
            $entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
            reset($entries);
            while (list(, $entry) = each($entries)) {
                $entry = trim($entry);
                if (preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list)) {
                    $private_ip = array(
                        '/^0\./',
                        '/^127\.0\.0\.1/',
                        '/^192\.168\..*/',
                        '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
                        '/^10\..*/');
                    $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
                    if ($client_ip != $found_ip) {
                        $client_ip = $found_ip;
                        break;
                    }
                }
            }
        } else {
            $client_ip = (!empty($_SERVER['REMOTE_ADDR']) ) ?
                    $_SERVER['REMOTE_ADDR'] :
                    ( (!empty($_ENV['REMOTE_ADDR']) ) ?
                            $_ENV['REMOTE_ADDR'] :
                            "unknown" );
        }
        return $client_ip;
    }

    public function getHorasSegundo($hora) {
        $arHora = explode(":", $hora);
        $hh1 = $arHora[0];
        $mm1 = $arHora[1];
        $ss1 = $arHora[2];
        $segundos = $hh1 * 3600 + $mm1 * 60 + $ss1;
        return $segundos;
    }

    public function getSegundoHoras($segun) {
        if ($segun < 0) {
            $segundos = $segun * (-1);
        } else {
            $segundos = $segun;
        }
        $var1 = $segundos / 3600;
        if (strpos($var1, ".")) {
            $hora = substr($var1, 0, strpos($var1, ".") - strlen($var1));
        } else {
            $hora = $var1;
        }
        $var2 = ($segundos - ($hora * 3600) ) / 60;
        if (strpos($var2, ".")) {
            $min = substr($var2, 0, strpos($var2, ".") - strlen($var2));
        } else {
            $min = $var2;
        }
        $seg = $segundos - ($hora * 3600) - ($min * 60);
        if ($hora < 10) {
            $horaf = "0$hora";
        } else {
            $horaf = $hora;
        }
        if ($min < 10) {
            $minf = "0$min";
        } else {
            $minf = $min;
        }
        if ($seg < 10) {
            $segf = "0$seg";
        } else {
            $segf = $seg;
        }
        return "$horaf:$minf:$segf";
    }

    public function registrarMarcacion($idoperador, $idtipo, $hora1, $idturno, $ipVisitante) {
//include_once './MDropDownList.php'; 
//include '../config/Config.php';
        $conf = new Config();
        $hora = $conf->getHoraActual();
        $seg_hora_act = $this->getHorasSegundo($hora);
        $ip = ""; //$this->getRealIP();
        if ($idtipo == 1) { //Ingreso
            $datos['idoperador'] = $idoperador;
            $datos['fecha_marcada'] = $conf->getFechaCortaActual();

            $idturno = $_SESSION['idhorario'];
            $horarios = $this->listarHorarios($idturno);
            $oMarcacion = $horarios['object'];
            print_r($oMarcacion);
            echo "<br>";
            $idmarcacion = $oMarcacion[0]['idmarcacion'];
            $h_ingreso_t = $oMarcacion[0]['h_ingreso'];
            $h_break_t = $oMarcacion[0]['h_break'];
            $h_backbreak_t = $oMarcacion[0]['h_backbreak'];
            $seg_h_ingreso_t = $this->getHorasSegundo($h_ingreso_t);
            $seg_h_break_t = $this->getHorasSegundo($h_break_t);
            $seg_h_backbreak_t = $this->getHorasSegundo($h_backbreak_t);
            $cond1 = $seg_h_ingreso_t + 900; //8:15
            $cond2 = $seg_h_ingreso_t + 2700; //8:45
            $cond3 = $seg_h_ingreso_t + 4500; //9:15
            $cond4 = $seg_h_break_t; // break en segundos
            $cond5 = $seg_h_backbreak_t; // backbreak en segundos
            $res_data = array(
                'cond1' => $cond1,
                'cond2' => $cond2,
                'cond3' => $cond3,
                'cond4' => $cond4,
                'cond5' => $cond5,
                'segun-actu' => $seg_hora_act,
            );
            print_r($res_data);
            if ($seg_hora_act < $cond1) { //si la hora marcada es menor a las 08:15
                $datos['begin_day'] = $h_ingreso_t;
                $datos['break'] = $h_ingreso_t;
                $datos['backbreak'] = $h_ingreso_t;
                $datos['end_day'] = $h_ingreso_t;
                $estadoMarcacion = 4;
//echo "Hora Marcada : $h_ingreso_t";
            } else if ($seg_hora_act >= $cond1 && $seg_hora_act < $cond2) { // si la hora es mayor a 8:15 y menos a 8:45
                $seg = $seg_h_ingreso_t + 1800;
                $horF = $this->getSegundoHoras($seg);
                $datos['begin_day'] = $horF;
                $datos['break'] = $horF;
                $datos['backbreak'] = $horF;
                $datos['end_day'] = $horF;
                $estadoMarcacion = 4;
//                echo "Hora Marcada : $horF";
//                exit();
            } else if ($seg_hora_act >= $cond2 && $seg_hora_act < $cond3) {// si la hora es mayor a 8:45 y menor a 9:15
                echo "hola ";
                $seg = $seg_h_ingreso_t + 3600;
                $horF = $this->getSegundoHoras($seg);
                $datos['begin_day'] = $horF;
                $datos['break'] = $horF;
                $datos['backbreak'] = $horF;
                $datos['end_day'] = $horF;
                $estadoMarcacion = 4;
//                echo "Hora Marcada : $horF";
//                exit();
            } else {
                $datos['begin_day'] = $hora;
                $datos['break'] = $hora;
                $datos['backbreak'] = $hora;
                $datos['end_day'] = $hora;
                if ($seg_hora_act >= $cond5) {
                    echo "back_break";
                    $datos['breakf'] = $hora;
                    $datos['backbreakf'] = $hora;
                    $estadoMarcacion = 6;
                } else if ($seg_hora_act >= $cond4) {
                    $datos['breakf'] = $hora;
                    $estadoMarcacion = 5;
                    echo "break";
                } else {
                    $estadoMarcacion = 4;
                }
            }
            echo "<br>" . $estadoMarcacion;
            $datos['idturno'] = $idturno;
            $datos['ip_marc_ing'] = $ipVisitante;
            $datos['hora_falsa'] = $hora1;
            $datos['begin_dayf'] = $hora;
            $datos['user_pc'] = get_current_user();
            $datos['dia'] = date("w");
            //exit();
            $cn = parent::sqlInsert($datos, 'marcacion');
            $lastID = $cn['lastID'];
            $_SESSION['idmarcacion'] = $lastID;
            $query = "UPDATE operador SET idmarcacion_est = '4' WHERE id_operador = " . $_SESSION['id_operador'];
            parent::insUpdDel($query);
            $_SESSION['idmarcacion_estado'] = $estadoMarcacion;
        } else if ($idtipo == 2) { // Break
            $oMarcacion = $this->getIdMarcacion();
            $idmarcacion = $oMarcacion[0]['idmarcacion'];
            $h_break_t = $oMarcacion[0]['h_break'];
            $h_backbreak_t = $oMarcacion[0]['h_backbreak'];
            $seg_h_break_t = $this->getHorasSegundo($h_break_t);
            $seg_h_backbreak_t = $this->getHorasSegundo($h_backbreak_t);
            if ($seg_hora_act > $seg_h_break_t) { //si la hora marcada es mayor a las 13
                $hora2 = $h_break_t;
            } else if ($seg_hora_act > $seg_h_backbreak_t) {// si el break es marcado despues de las 14
                $hora2 = $h_break_t;
            } else {
                $hora2 = $hora;
            }
            $query = "UPDATE marcacion SET break = '$h_break_t', backbreak = '$h_backbreak_t', end_day = '$hora2', breakf = '$hora', ip_marc_bre = '$ipVisitante' WHERE idmarcacion = '$idmarcacion'";
            parent::insUpdDel($query);
            $query1 = "UPDATE operador SET idmarcacion_est = '5' WHERE id_operador = " . $_SESSION['id_operador'];
            parent::insUpdDel($query1);
            $_SESSION['idmarcacion_estado'] = 5;
        } else if ($idtipo == 3) { // Fin Break
            $oMarcacion = $this->getIdMarcacion();
            $idmarcacion = $oMarcacion[0]['idmarcacion'];
            $h_backbreak_t = $oMarcacion[0]['h_backbreak'];
            $seg_h_backbreak_t = $this->getHorasSegundo($h_backbreak_t);
            if ($seg_hora_act < $seg_h_backbreak_t) { //si la hora marcada es menor a las 14
                $hora2 = $h_backbreak_t;
            } else {
                $hora2 = $hora;
            }
            $query = "UPDATE marcacion SET backbreak = '$h_backbreak_t', end_day = '$hora2', backbreakf = '$hora', ip_marc_bbk = '$ipVisitante' WHERE idmarcacion = '$idmarcacion'";
            parent::insUpdDel($query);
            $query1 = "UPDATE operador SET idmarcacion_est = '6' WHERE id_operador = " . $_SESSION['id_operador'];
            parent::insUpdDel($query1);
            $_SESSION['idmarcacion_estado'] = 6;
        } else if ($idtipo == 4) { // Salida
            $oMarcacion = $this->getIdMarcacion();
            $idmarcacion = $oMarcacion[0]['idmarcacion'];
            $h_salida_t = $oMarcacion[0]['h_salida'];
            $seg_h_salida_t = $this->getHorasSegundo($h_salida_t);
            if ($seg_hora_act > $seg_h_salida_t) { //si la hora marcada es mayor a las 18
                $hora2 = $h_salida_t;
            } else { //si marca antes de su hora de salida redondear la hora de salida                
                $h = $conf->getFechaFormat("H:00:00");
                $h_seg = $conf->getHorasSegundo($h);
                if ($seg_hora_act < $h_seg + 1200) { // hora antes de los 15
                    $hora2 = $h;
                } else if ($seg_hora_act > $h_seg + 3000) { // despues de las 45
                    $hora2 = $conf->getSegundoHoras($h_seg + 3600);
                } else { // entre 15 y 15
                    $hora2 = $conf->getSegundoHoras($h_seg + 1800);
                }
            }
            $query = "UPDATE marcacion SET end_day = '$hora2', end_dayf = '$hora', ip_marc_end = '$ipVisitante' WHERE idmarcacion = '$idmarcacion'";
            parent::insUpdDel($query);
            $query1 = "UPDATE operador SET idmarcacion_est = '7' WHERE id_operador = " . $_SESSION['id_operador'];
            parent::insUpdDel($query1);
            $_SESSION['idmarcacion_estado'] = 7;
        }
        return;
    }

    public function mostrarDetalleMarcacion($idmarcacion) {
        $query = "select * from marcacion where idmarcacion = $idmarcacion ";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function mostrarRegAsistenciaByID($idOperador, $tipo, $fecIni, $fecFin) {
        $conf = new Config();
        $mes = $conf->getFechaFormat("m");
        if ($tipo == 1) {
            $cond = " month(fecha_marcada) = month(now()) ";
        } else if ($tipo == 2) {
            $cond = " fecha_marcada between '$fecIni' and '$fecFin' ";
        }
        $query = "select m.idmarcacion, m.idturno as idhorarioM, ho.descripcion horario, m.idoperador, concat(nombre, ' ', apepa, ' ', apema ) nombres, se.descripcion sede, m.fecha_marcada, 
        m.begin_day, m.break, m.backbreak, m.end_day,o.idtipo_usuario, m.begin_dayf, m.breakf, m.backbreakf, m.end_dayf,o.dni,m.dia,
         TIMEDIFF( m.begin_day, hm.h_ingreso)  tardanza,
         TIME_TO_SEC(TIMEDIFF( m.begin_day, hm.h_ingreso)) tard_seg,
         TIMEDIFF( m.begin_dayf, hm.h_ingreso) tardanzaf,
         TIME_TO_SEC(TIMEDIFF( m.begin_dayf, hm.h_ingreso)) tard_segf,
         TIMEDIFF(TIMEDIFF( m.end_day, m.begin_day),TIMEDIFF( m.backbreak, m.break)) asistencia, 
         TIMEDIFF( m.backbreak, m.break) almuerzo,
         TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)) almuerzo_seg,
         timediff(TIMEDIFF( m.end_day, m.begin_day), TIMEDIFF( m.backbreak, m.break)) horas_trabajo,
         TIME_TO_SEC(timediff(TIMEDIFF( m.end_day, m.begin_day), TIMEDIFF( m.backbreak, m.break))) horas_trabajo_seg,
         o.idturno, t.descripcion turno, ip_marc_ing, ip_marc_bre, ip_marc_bbk, ip_marc_end,
         hm.h_ingreso, hm.h_break, hm.h_backbreak, hm.h_salida, o.idmarcacion_est , e.descripcion estMarcacion
        from marcacion m
        inner join operador o on o.id_operador = m.idoperador
        inner join turno t on o.idturno = t.idturno 
        inner join estado e on e.idestado = o.idmarcacion_est
        inner join sede se on se.idsede = o.idsede
        inner join horario ho on ho.idhorario = m.idturno
        inner join horario_marc hm on hm.idhorario = ho.idhorario and hm.dia = m.dia
        where m.idoperador = $idOperador and  $cond
        group by (fecha_marcada)
        order by fecha_marcada desc;";
        //echo $query;
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function mostrarHorasTrabajadasxMes() {
        $query = "select m.idmarcacion, m.dia, m.idoperador, m.fecha_marcada,  
        m.idturno as idhorarioM ,o.dni, o.idtipo_usuario, se.descripcion sede,
        concat(nombre, ' ', apepa, ' ', apema ) nombres, o.idturno, o.idarea , o.idsede , 
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_day, hm.h_ingreso)))) tardanza,
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_dayf, hm.h_ingreso)))) tardanzaf,
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia,
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo,
        TIMEDIFF( 
         SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))),
         SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) 
        ) as total_horas
        from marcacion m 
        inner join operador o on o.id_operador = m.idoperador
        inner join sede se on se.idsede = o.idsede
        inner join horario ho on ho.idhorario = o.idhorario
        inner join horario_marc hm on ho.idhorario = hm.idhorario and m.dia = hm.dia
        where  month(fecha_marcada) = month(now())  and o.idestado = 1 and m.idestado != 3
        group by m.idoperador
        order by o.idestado,o.idsede,o.idturno,nombre";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function mostrarHorasTrabajadasxDia() {
        $conf = new Config();
        $dia = $conf->getFechaFormat("w");
        $query = "select m.idmarcacion, m.idturno, m.idoperador, concat(nombre, ' ', apepa, ' ', apema ) nombres,
        se.descripcion sede, m.fecha_marcada,  o.idtipo_usuario, o.idarea, o.idestado,ho.idhorario,
        m.begin_day, m.break, m.backbreak, m.end_day,o.idtipo_usuario, m.begin_dayf, m.breakf, m.backbreakf, o.dni,
        TIMEDIFF( m.begin_day, hm.h_ingreso) tardanza, 
        TIME_TO_SEC(TIMEDIFF( m.begin_day, hm.h_ingreso)) tard_seg,
        TIMEDIFF( m.begin_dayf, hm.h_ingreso) tardanzaf, 
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia, 
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo, 
        timediff(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))), 
        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break))))) horas_trabajo, 
        m.end_dayf, hm.h_ingreso, hm.h_break, hm.h_backbreak, hm.h_salida, o.idmarcacion_est , e.descripcion estMarcacion
        from marcacion m
        inner join operador o on o.id_operador = m.idoperador
        inner join turno t on m.idturno = t.idturno 
        inner join estado e on e.idestado = o.idmarcacion_est
        inner join sede se on se.idsede = o.idsede
        inner join horario ho on ho.idhorario = m.idturno
        inner join horario_marc hm on hm.idhorario = ho.idhorario
        where month(fecha_marcada) = month(now()) and day(fecha_marcada) = day(now()) and o.idestado = 1 and hm.dia = $dia
        group by m.idoperador
        order by o.idsede,o.idturno,nombre; ";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function restablecereMarcacion($tipoTurno) {
        if ($tipoTurno == 1) {
            $query = "call reload_user_login_ausent";
        } else {
            $query = "";
        }
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function quitar($tabla, $attributo, $id) {
        $query = "UPDATE $tabla SET idestado = 2 WHERE $attributo = '$id'";
        $cn = parent::insUpdDel($query);
        return $cn;
    }

    public function habilitar($tabla, $attributo, $id) {
        $query = "UPDATE $tabla SET idestado = 1 WHERE $attributo = '$id'";
        $cn = parent::insUpdDel($query);
        return $cn;
    }

    public function actualizarMarcacion($dat) {

        $datos['begin_day'] = $dat['h_ingreso'];
        $datos['break'] = $dat['h_break'];
        $datos['backbreak'] = $dat['h_backbreak'];
        $datos['end_day'] = $dat['h_egreso'];
        $datos['begin_dayf'] = $dat['h_ingresof'];
        $datos['breakf'] = $dat['h_breakf'];
        $datos['backbreakf'] = $dat['h_backbreakf'];
        $datos['end_dayf'] = $dat['h_egresof'];
        $datos['idmarcacion'] = $dat['idmarcacion'];

        $cn = parent::sqlUpdate($datos, 'marcacion');
        $status = $cn['status'];

        if ($status == "ok") {
            $datos1['begin_day'] = $dat['h_ingreso'];
            $datos1['break'] = $dat['h_break'];
            $datos1['backbreak'] = $dat['h_backbreak'];
            $datos1['end_day'] = $dat['h_egreso'];
            $datos1['begin_dayf'] = $dat['h_ingresof'];
            $datos1['breakf'] = $dat['h_breakf'];
            $datos1['backbreakf'] = $dat['h_backbreakf'];
            $datos1['end_dayf'] = $dat['h_egresof'];
            $datos1['idmarcacion'] = $dat['idmarcacion'];
            $datos1['h_ingO'] = $dat['h_ingO'];
            $datos1['h_brkO'] = $dat['h_brkO'];
            $datos1['h_bbkO'] = $dat['h_bbkO'];
            $datos1['h_egrO'] = $dat['h_egrO'];
            $datos1['h_ingFO'] = $dat['h_ingFO'];
            $datos1['h_brkFO'] = $dat['h_brkFO'];
            $datos1['h_bbkFO'] = $dat['h_bbkFO'];
            $datos1['h_egrFO'] = $dat['h_egrFO'];
            $datos1['idmarcacion'] = $dat['idmarcacion'];
            $datos1['motivo'] = $dat['motivo'];
            $datos1['fecha_mod'] = $dat['fecha_mod'];
            $datos1['hora_mod'] = $dat['hora_mod'];
            $datos1['idoperador'] = $dat['idpersona'];
            $cn1 = parent::sqlInsert($datos1, 'reg_mod_marcacion');
            return $cn1;
        } else {
            return $cn;
        }
    }

}
