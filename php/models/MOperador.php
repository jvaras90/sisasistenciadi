<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MOperador
 *
 * @author alucard
 */
include '../config/CConexion.php';
//session_start();

class MOperador extends CConexion {

    public function __construct() {
        parent::CConectarse();
    }

    public function listarOperadores() {
        $dia = date("w");
        $query = "select  o.id_operador, o.nombre , apepa , apema , f_ing , f_egr , dni , o.idhorario,
            o.foto, o.idturno , t.descripcion turno, o.idarea, a.descripcion area , 
            o.idtipo_usuario, tu.descripcion tipo_usuario , o.idestado , e.descripcion estado , 
            o.idmarcacion_est , em.descripcion Marcacion , o.idsede, se.descripcion sede
            from operador o
            inner join turno t on t.idturno = o.idturno
            inner join area a on a.idarea = o.idarea 
            inner join tipo_usuario tu on tu.idtipo_usuario = o.idtipo_usuario
            inner join estado e on e.idestado = o.idestado
            inner join estado em on em.idestado = o.idmarcacion_est
            inner join sede se on se.idsede = o.idsede
            inner join horario ho on ho.idhorario = o.idhorario
            inner join horario_marc hm on ho.idhorario = hm.idhorario
            where hm.dia = $dia
            order by o.idestado , o.idsede, o.idturno,nombre  ; ";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function detalleOperador($id) {
        $dia = date("w");
        $query = "select  o.id_operador, o.nombre , apepa , apema , f_ing , f_egr , dni , telefono,email,direccion,
            o.foto, o.idturno , t.descripcion turno, o.idarea, a.descripcion area , 
            o.idtipo_usuario, tu.descripcion tipo_usuario , o.idestado , e.descripcion estado , 
            o.idmarcacion_est , em.descripcion Marcacion, o.idhorario,
            hm.h_ingreso,hm.h_break,hm.h_backbreak,hm.h_salida
            from operador o
            inner join turno t on t.idturno = o.idturno
            inner join area a on a.idarea = o.idarea 
            inner join tipo_usuario tu on tu.idtipo_usuario = o.idtipo_usuario
            inner join estado e on e.idestado = o.idestado
            inner join estado em on em.idestado = o.idmarcacion_est
            inner join sede se on se.idsede = o.idsede
            inner join horario ho on ho.idhorario = o.idhorario
            inner join horario_marc hm on ho.idhorario = hm.idhorario
            where hm.dia = $dia and o.id_operador = $id";
        $cn = parent::consultasLibres($query);
        return $cn;
    }

    public function registrarOperador($dat) {
        $datos['dni'] = $dat['dni'];
        $datos['nombre'] = utf8_encode($dat['nombre']);
        $datos['apepa'] = utf8_encode($dat['apepa']);
        $datos['apema'] = utf8_encode($dat['apema']);
        $datos['idarea'] = $dat['idarea'];
        $datos['idturno'] = $dat['idturno'];
        $datos['idtipo_usuario'] = $dat['idtipo_usuario'];
        $datos['f_ing'] = date("Y-m-d");
        //$datos[''] = $dat[''];
        $cn = parent::sqlInsert($datos, 'operador');
        return $cn;
    }

    public function actualizarOperador($dat) {
        if (isset($dat['idarea'])) {
            $datos['dni'] = $dat['dni'];
        }
        $datos['nombre'] = utf8_encode($dat['nombre']);
        $datos['apepa'] = utf8_encode($dat['aPaterno']);
        $datos['apema'] = utf8_encode($dat['aMaterno']);
        $datos['telefono'] = utf8_encode($dat['telefono']);
        $datos['email'] = utf8_encode($dat['email']);
        $datos['direccion'] = utf8_encode($dat['direccion']);
        $datos['f_ing'] = $dat['f_ingreso'];
        $datos['f_egr'] = $dat['f_egreso'];
        if (isset($dat['idarea'])) {
            $datos['idarea'] = $dat['idarea'];
        }
        if (isset($dat['idturno'])) {
            $datos['idturno'] = $dat['idturno'];
        }
        if (isset($dat['idtipo_usuario'])) {
            $datos['idtipo_usuario'] = $dat['idtipo_usuario'];
        }
        if (isset($dat['idestado'])) {
            $datos['idestado'] = $dat['idestado'];
        }
        if (isset($dat['idestadoMarcacion'])) {
            $datos['idmarcacion_est'] = $dat['idestadoMarcacion'];
        }
        if (isset($dat['idhorario'])) {
            $datos['idhorario'] = $dat['idhorario'];
        }
        $datos['id_operador'] = $dat['id_operador'];
        //$datos[''] = $dat[''];
        $cn = parent::sqlUpdate($datos, 'operador');
        return $cn;
    }

    public function modificarFoto($datos) {
        $idOperador = $datos['idOperador'];
        $dni = $datos['dniOperador'];
        //exit();
        if (empty($_FILES["foto_fls"]["tmp_name"])) {
            $imagen = $datos["foto_hdn"];
        } else {//se ejecuta la funcion para subir la imagen
            include ('imagenUpload.php');
            $tipo = $_FILES["foto_fls"]["type"];
            $archivo = $_FILES["foto_fls"]["tmp_name"];
            echo "tipo: $tipo <br>";
            echo "archivo: $archivo";
            //exit();
            $imagen = subir_imagen($tipo, $archivo, $dni);
            //exit();
        }
        $query = "UPDATE operador SET foto='$imagen' WHERE id_operador='$idOperador'";
        $cn = parent::insUpdDel($query);
        return $cn;
    }

}
