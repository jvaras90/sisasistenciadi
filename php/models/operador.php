<?php

class operador {

    private $id_operador;
    private $nombre;
    private $apepa;
    private $apema;
    private $f_ing;
    private $f_egr;
    private $dni;
    private $Id_Tipo;

    function __construct() {
        
    }

    function operador() {
        
    }

    function getId_operador() {
        return $this->id_operador;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApepa() {
        return $this->apepa;
    }

    function getApema() {
        return $this->apema;
    }

    function getF_ing() {
        return $this->f_ing;
    }

    function getF_egr() {
        return $this->f_egr;
    }

    function getDni() {
        return $this->dni;
    }

    function getId_Tipo() {
        return $this->Id_Tipo;
    }

    function setId_operador($id_operador) {
        $this->id_operador = $id_operador;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApepa($apepa) {
        $this->apepa = $apepa;
    }

    function setApema($apema) {
        $this->apema = $apema;
    }

    function setF_ing($f_ing) {
        $this->f_ing = $f_ing;
    }

    function setF_egr($f_egr) {
        $this->f_egr = $f_egr;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setId_Tipo($Id_Tipo) {
        $this->Id_Tipo = $Id_Tipo;
    }

}
