<?php
$config = new Config();
//print_r($arrayOperador);
?>
<div class="row">

    <div class="col-md-4 col-sm-5 col-xs-8">
        <div class="x_panel">
            <div class="x_title">
                <h2>Actualizar Imagen</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li>
                        <a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
                    <?php
                    if (isset($_GET['msj'])) {
                        echo "<div class='label label-warning'>$mensaje</div>";
                    }
                    ?>   
                    <div class="profile_img">

                        <!-- end of image cropping -->
                        <div id="crop-avatar">
                            <!-- Current avatar -->
                            <div  data-original-title="Cambiar Imagen" class="avatar-view" title="">
                                <img src="images/Fotos/<?php echo $arrayOperador[0]['foto']; ?>" alt="Avatar">
                            </div> 
                            <!-- Cropping modal -->
                            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <form class="avatar-form" action="" enctype="multipart/form-data" method="post" id="formModificarFoto" name="formModificarFoto"> 
                                            <input type="hidden" name="controlador" value="operador"> 
                                            <input type="hidden" name="funcion" value="modificarFoto"> 
                                            <div class="modal-header">
                                                <button class="close" data-dismiss="modal" type="button">×</button>
                                                <h4 class="modal-title" id="avatar-modal-label">Cambiar Foto de Perfil</h4>
                                                <input class="hidden" type="text" id="idOperador" name="idOperador" value="<?php echo $arrayOperador[0]['id_operador']; ?>"/>
                                                <input class="hidden" type="text" id="dniOperador" name="dniOperador" value="<?php echo $arrayOperador[0]['dni']; ?>"/>
                                            </div>
                                            <div class="modal-body">
                                                <div class="avatar-body">
                                                    <!-- Upload image and data -->
                                                    <div class="avatar-upload">
                                                        <input class="avatar-src" name="avatar_src" type="hidden">
                                                        <input class="avatar-data" name="avatar_data" type="hidden">
                                                        <label for="foto">Subir Imagen</label>
                                                        <input class="avatar-input"  id="foto_fls" name="foto_fls" type="file">
                                                        <input type="hidden" name="foto_hdn" value="<?php echo $arrayOperador[0]['foto']; ?>">
                                                    </div>

                                                    <!-- Crop and preview -->
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="avatar-wrapper"></div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="avatar-preview preview-lg"></div>
                                                            <div class="avatar-preview preview-md"></div>
                                                            <div class="avatar-preview preview-sm"></div>
                                                        </div>
                                                    </div>

                                                    <div class="row avatar-btns">
                                                        <div class="col-md-9">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button   class="btn btn-primary btn-block avatar-save" type="submit">Guardar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <script>
                                    $('#formModificarFoto').submit(function (e) {
                                        e.preventDefault();
                                        //var foto = $('#foto_fls').val();
                                        //var datos = $('#formModificarFoto').serialize();
                                        var data = new FormData(this);
                                        $.ajax({
                                            url: 'index.php',
                                            type: 'POST',
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            data: data,
                                            success: function (datos) {
                                                //var msj = datos.innerHTML;
                                                alert("Deberá Reiniciar session para ver los cambios");
                                                salir('<?php echo $config->getConfig()['urlBase']; ?>');
                                            }
                                        });
                                    });
                                </script>
                            </div>
                            <!-- /.modal --> 
                            <!-- Loading state -->
                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                        </div>
                        <!-- end of image cropping -->
                    </div>
                    <h3><?php echo $arrayOperador[0]['nombre'] . " " . $arrayOperador[0]['apepa'] . " " . $arrayOperador[0]['apema']; ?></h3>

                    <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo "Lima"; ?>
                        </li>
                        <li>
                            <i class="fa fa-briefcase user-profile-icon"></i> Cargo: <?php echo $arrayOperador[0]['area']; ?>
                        </li>
                    </ul> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-7 col-xs-12">
        <form class="form-horizontal form-label-left" id="formActualizarOperador" name="formActualizarOperador" method="post" action="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Datos Personales <?php
                            if ($_SESSION['id_nivel'] == 1) {
                                echo $arrayOperador[0]['id_operador'];
                            }
                            ?></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Nombre</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="nombre">
                                    <input placeholder="Inrese sus nombres" name="nombre" id="nombre" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text" value="<?php echo utf8_decode($arrayOperador[0]['nombre']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" >
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">N° Documento</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="cliente">
                                    <input placeholder="Ingrese su DNI"  name="dni" id="dni" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text" value="<?php echo $arrayOperador[0]['dni']; ?>" <?php
                                    if ($_SESSION['id_nivel'] == 3) {
                                        echo "readonly";
                                    }
                                    ?>>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Paterno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aPaterno">
                                    <input placeholder="Ingrese su apellido paterno"   name="aPaterno" id="aPaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo utf8_decode($arrayOperador[0]['apepa']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">A. Materno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="aMaterno">
                                    <input placeholder="Ingrese su apellido materno"   name="aMaterno" id="aMaterno" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo utf8_decode($arrayOperador[0]['apema']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Teléfono</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="telefono">
                                    <input placeholder="999 999 999"  name="telefono" id="telefono" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo utf8_decode($arrayOperador[0]['telefono']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">E-mail</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="email">
                                    <input placeholder="aaa@ejemplo.com"   name="email" id="email" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo utf8_decode($arrayOperador[0]['email']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Dirección</label>
                                <div class="col-md-10 col-sm-10 col-xs-10" id="direccion">
                                    <input placeholder="domicilio - distrito - ciudad"   name="direccion" id="direccion" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo utf8_decode($arrayOperador[0]['direccion']); ?>" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();">
                                </div> 
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Ingreso</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="f_ingreso">
                                    <input  name="f_ingreso" id="f_ingreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['f_ing']; ?>" <?php
                                    if ($_SESSION['id_nivel'] == 3) {
                                        echo "readonly";
                                    }
                                    ?>>
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Fecha Egreso</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="f_egreso">
                                    <input   name="f_egreso" id="f_egreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['f_egr']; ?>" <?php
                                    if ($_SESSION['id_nivel'] == 3) {
                                        echo "readonly";
                                    }
                                    ?>>
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Area</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpe_area">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Horario</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpe_horario">
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Ingreso</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="h_ingreso">
                                    <input  name="h_ingreso" id="h_ingreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['h_ingreso']; ?>" readonly="">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Salida</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="h_egreso">
                                    <input   name="h_egreso" id="h_egreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['h_salida']; ?>" readonly="">
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Break</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="h_break">
                                    <input  name="h_break" id="h_break" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['h_break']; ?>" readonly="">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Regreso</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="h_backbreak">
                                    <input   name="h_backbreak" id="h_backbreak" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayOperador[0]['h_backbreak']; ?>" readonly="">
                                </div>
                            </div> 
                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Tipo User</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpe_tipo_user">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Estado</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpe_estado">
                                </div>
                            </div> 

                            <div class="item form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Marcación</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpeEstado_marcacion">
                                </div>
                                <label class="control-label col-md-2 col-sm-2 col-xs-2">Turno</label>
                                <div class="col-md-4 col-sm-4 col-xs-4" id="divOpe_turno">
                                </div>
                            </div> 

                            <div class="hidden">
                                <input type="hidden" name="id_operador" id="id_operador" value="<?php echo $arrayOperador[0]['id_operador']; ?>">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-footer">
                            <div style="text-align: center;">
                                <a onclick="<?php
                                if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') {
                                    echo "listadoOperadores();";
                                } else {
                                    echo "index();";
                                }
                                ?>" class="btn btn-danger"><i class="fa fa-rotate-left"></i> Regresar</a>
                                <a onclick="actualizarOperadores();" class="btn btn-primary"><i class="fa fa-rotate-left"></i> Actualizar</a>
                                <!--<input type="button" value="Actualizar" id="actualizarPersonal" name="actualizarPersonal" class="btn btn-primary" onclick="validarFormPersona(this)"/>-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div> 
</div>
<div class="row">
    <div class="col-md-12">
        <div class = "x_panel" id="divMostrarResultados">

        </div>
    </div>
</div>
<script>
    asistenciaPersonalById(<?php echo $arrayOperador[0]['id_operador']; ?>);
</script>
<!-- image cropping -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/cropping/cropper.min.js"></script>
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/cropping/main.js"></script>


