<!DOCTYPE html>
<html>
    <?php include '../../config/Config.php'; $config = new Config(); ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> <?php echo $config->getConfig()['nombreSis'] . " | " . $config->getConfig()['empresa'] ?></title>
        <link href="images/logo.ico" rel="shortcut icon" />
        <!-- Bootstrap core CSS -->

        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $config->getConfig()['urlBase'] ?>fonts/css/font-awesome.min.css" rel="stylesheet"> 
        <!-- Custom styling plus plugins -->
        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/custom.css" rel="stylesheet"><link href="<?php echo $config->getConfig()['urlBase'] ?>css/icheck/flat/green.css" rel="stylesheet" />

        <script src="<?php echo $config->getConfig()['urlBase'] ?>js/js.js" type="text/javascript"></script>
        <script src="<?php echo $config->getConfig()['urlBase'] ?>js/jquery.min.js"></script>
        <style>
            .errorImput1{ 
                border: 1px solid #e80c4d;
                font-weight: bold;
                font-size: 1.1em;
            }
        </style>
    </head>
    <body style="background-color: whitesmoke">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="top_nav">

                    </div>                        
                </div>
                <div class="right_col" role="main">

                    <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <u>Contacto con Sistemas</u></h1>
                    <form style="text-align: center;">
                        <h3>Romulo Puppi</h3>
                        <p><b>Celular:</b> 940151636 <br>
                            <b>Correo:</b> ropuppi@dataimage-pro.com
                        </p>
                        <h3>Jean Harold Varas Ramirez</h3>
                        <p><b>Celular:</b> 945019336 <br>
                            <b>Correo:</b> thewiner57@gmail.com
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>