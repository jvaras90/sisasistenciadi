<?php

$h_ingreso = '08:00:00';
$h_break = '13:00:00';
$h_backbreak = '14:00:00';
$h_salida = '18:00:00';
$cadena = "";
for ($dia = 1; $dia <= 5; $dia++) {

    $cadena .= '<tr id="' . $dia . '" class="impreso">
        <td><input type="hidden" name="numDia' . $dia . '" value="' . $dia . '"/>' . $dia . '</td>
        <td><input type="hidden" name="ingreso' . $dia . '" value="' . $h_ingreso . '"/>' . $h_ingreso . '</td>
        <td><input type="hidden" name="break' . $dia . '" value="' . $h_break . '"/>' . $h_break . '</td>
        <td><input type="hidden" name="bkbreak' . $dia . '" value="' . $h_backbreak . '"/>' . $h_backbreak . '</td>
        <td><input type="hidden" name="salida' . $dia . '" value="' . $h_salida . '"/>' . $h_backbreak . '</td>
        <td><a class="btn-sm btn-round  btn-danger" onclick="eliminarHora(' . $dia . ');"><i class="fa fa-minus"></i></a></td>
    </tr>';
}
echo htmlspecialchars($cadena);
