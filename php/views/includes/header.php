<!DOCTYPE html>
<?php
include '../config/Config.php';
//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
session_start();
//clearstatcache();
require('includes_head.php');
if ($_SESSION['dni_user'] == "") {
    header('Location: index.php');
}
$userAgent = "";
$userAgent = $_SERVER[HTTP_USER_AGENT];
$userAgent1 = strtolower($userAgent);
//echo $userAgent1;
$color = "";
if (strpos($userAgent1, "android") !== false || strpos($userAgent1, "iphone") !== false) {
    $mobil = 1;
    $newDimensionNav = "width: 1000px;";
    $newDimensionBody = 'style="' . $newDimensionNav . '"';
} else {
    $mobil = 0;
    $newDimensionNav = "";
    $newDimensionBody = "";
}
/*
function funcObtener() {
    echo "ip: " . $_SERVER['HTTP_CLIENT_IP'] . "<br>";
    echo "IP Proxy: " . $_SERVER['HTTP_USER_AGENT'] . "<br>";
    echo "IP Proxy: " . $_SERVER['HTTP_X_FORWARDED_FOR'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_X_FORWARDED'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_FORWARDED_FOR'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_FORWARDED'] . "<br>";
    echo "IP Access: " . $_SERVER['REMOTE_ADDR'] . "<br>";
}*/

//ar_dump($arrayMes);
?>
<body  style="<?php echo $newDimensionNav; ?>;background-color: <?php echo $color; ?>"  class="nav-md">

    <div class="container body">
        <div class="main_container"> 
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title"><i class="fa fa-empire"></i> <span><?php echo Config::$CONFIG['nombreSis']; ?></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Bienvenido(a),</span>
                            <h2><?php echo $_SESSION['nombres']; ?></h2>
                            <small class="label"><?php echo $_SESSION['nivel']; ?></small>
                        </div>
                    </div>
                    <!-- /menu prile quick info --> 
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">                            

                            <ul class="nav side-menu">
                                <h3>Configuración</h3>
                                <li>
                                    <a onclick="index('<?php echo Config::$CONFIG['urlBase']; ?>')"><i class="fa fa-home"></i> Principal</a>
                                </li>
                                <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                    <li><a><i class="fa fa-user"></i>Operadores <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a onclick="listadoOperadores();">Lista de Operadores</a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>      
                                <h3> Administracion y Gestión </h3>
                                <li>
                                    <a><i class="fa fa-clock-o"></i> Marcación <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none"> 
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i> Gestión</label>
                                        <?php if ($_SESSION['id_nivel'] == 1) { ?>
                                            <li><a onclick="">Tickets Generados</a>
                                            </li>
                                        <?php } ?>
                                        <li><a onclick="">Mis Registros</a>
                                        </li> 
                                    </ul>
                                </li>
                                <?php
                                if ($_SESSION['id_nivel'] == '1') {
                                    # code...
                                    ?>
                                    <li><a><i class="fa fa-exchange"></i> Pruebas<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="ejemploA"> Ejemplo de leer archivos</a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>

                            <ul class="nav side-menu">

                            </ul>                            
                        </div>
                        <div class="menu_section">

                        </div>
                    </div>
                    <!-- /sidebar menu -->
                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a href="" data-toggle="tooltip" data-placement="top" title="Salir" onclick="salir('<?php echo Config::$CONFIG['urlBase']; ?>')">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right"> 
                            <li class="">
                                <!--<label class="label-primary center-block">Hola</label>-->
                                <!--<h4 class="label-primary">Hola</h4>-->


                            </li>
                            <li class=""> 
                                <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>"><?php echo $_SESSION['nombres']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <?php $id_personal = $_SESSION['idpersonal']; ?>
                                        <a onclick="listarDetOperador(<?php echo $_SESSION['id_operador']; ?>)">  Perfil</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span><?php echo $_SESSION['area']; ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="php/doc/ayuda.php" target="_blank" onclick="window.open(this.href, this.target, 'width=1100,height=700,top=0,left=700,resizable=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes');
                                                return false;">Ayuda</a>
                                    </li>
                                    <li><a onclick="salir('<?php echo Config::$CONFIG['urlBase']; ?>')"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li> 
                            <li class="">
                                <a href="#">
                                    <!--<i class="fa fa-power-off"></i>-->
                                    <?php
                                    $arrayMes = array('1' => "Enero", '2' => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Setiembre", 10 => "Octubre", 11 => "Nobiembre", 12 => "Diciembre",);

                                    $localtime = localtime();
                                    $localtime_assoc = localtime(time(), true);
                                    $dia = $localtime['3'];
                                    $mes = $localtime['4'] + 1;
                                    $año = $localtime['5'] + 1900;
                                    echo $mes . "<br>";
                                    echo 'Fecha: ' . $dia . ' / ' . $arrayMes[$mes] . ' / ' . $año . '&nbsp;&nbsp;  ';
                                    ?>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            <div class="right_col" role="main">

                <div class="row">