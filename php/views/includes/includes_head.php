<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> <?php echo $config->getConfig()['nombreSis'] . " | " . $config->getConfig()['empresa'] ?></title>
    <link href="images/logo.ico" rel="shortcut icon" />
    <!-- Bootstrap core CSS -->

    <link href="<?php echo $config->getConfig()['urlBase'] ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $config->getConfig()['urlBase'] ?>fonts/css/font-awesome.min.css" rel="stylesheet"> 

    <link href="<?php echo $config->getConfig()['urlBase'] ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo $config->getConfig()['urlBase'] ?>css/icheck/flat/green.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo $config->getConfig()['urlBase'] ?>js/js.js"></script>
    <script src="<?php echo $config->getConfig()['urlBase'] ?>js/jquery.min.js"></script>
    <style>
        .errorImput1{ 
            border: 1px solid #e80c4d;
            font-weight: bold;
            font-size: 1.1em;
        }
    </style>
</head>