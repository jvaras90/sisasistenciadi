<?php
//session_start();
clearstatcache();
include_once'../config/Config.php';
$config = new Config();
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sis. Asistencia | DataImage SAC </title>
        <link href="images/logo.ico" rel="shortcut icon" />

        <!-- Bootstrap core CSS -->

        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo $config->getConfig()['urlBase'] ?>fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/custom.css" rel="stylesheet">
        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/icheck/flat/green.css" rel="stylesheet">
        <script src="<?php echo $config->getConfig()['urlBase'] ?>js/jquery.min.js"></script> 

        <!--<script src="<?php echo $config->getConfig()['urlBase'] ?>js/functions.ajax.js" type="text/javascript"></script>-->
    </head>

    <body style="background:#F7F7F7;">

        <div class="">
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>

            <div id="wrapper"> 
                <div id="login" class="animate form">

                    <div style="text-align: center;">
                        <img style="height: 120px;" src="<?php echo $config->getConfig()['urlBase'] ?>php/views/images/dataimagelogo.png">
                    </div> 
                    <section class="login_content"> 
                        <form id="formulario" method="POST">
                            <h1>Acceso al sistema</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="nro documento" id="login_nroDocumento" name="login_nroDocumento" required="" />
                            </div> 
                            <div>
                                <span class="timer" id="timer"></span>
                                <button class="btn-lg btn-primary" id="login_userbttn">Entrar</button>
                            </div>
                            <div class="clearfix"></div><br>

                            <?php
                            if ($mensaje != '') {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                    <?php echo $mensaje; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="separator">

                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-newspaper-o" style="font-size: 26px;"></i> DATAIMAGE S.A.C</h1>
                                    <p>©2017 Todos los derechos reservados. Dataimage SAC</p>
                                </div>
                            </div> 
                        </form>
                        <!-- content -->
                </div> 
            </div>
        </div>
        <script>
//            $(function () {
//                $('input').iCheck({
//                    checkboxClass: 'icheckbox_square-blue',
//                    radioClass: 'iradio_square-blue',
//                    increaseArea: '20%' // optional
//                });
//            });

            function ingresar() {
                // var clave = $('#password').val();
                //alert(clave);
                ////clave = calcMD5(clave);
                //$('#password').val(clave);
                $('#formulario').submit();

            }
        </script>
    </body>

</html>