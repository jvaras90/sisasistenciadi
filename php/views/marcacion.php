<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="center-block" id="miReloj" >

    </div>
    <input type="hidden" name="valReloj" id="valReloj" value="1"/>
    <br>
    <div class="center-block">
        <?php
        //echo getRealIP() ."<br>";
        //funcObtener();
        ?>
    </div>
</div>
<div class = "clearfix"></div>
<div class = "col-md-12 col-sm-12 col-xs-12">
    <div class = "x_panel">
        <h4 style = "text-align: center;">Horarios</h4>
        <div class = "x_title">
            <div id = "divListHorarios">
            </div>
            <div class = "clearfix"></div>
        </div>
        <div class = "x_content">
            <input type="hidden" name="ipVisitante" id="ipVisitante">
            <?php
            //echo "Marcacion Estado ; ".$_SESSION['idmarcacion_estado'];
            if ($_SESSION['mobil'] == 0) {
                //echo "<h1>Mobil : " . $_SESSION['mobil'] . "</h1>";
                ?>
                <?php if ($_SESSION['idmarcacion_estado'] == 3) { ?>
                    <div class="row" style="text-align: center;">
                        <input style="
                               margin: 10px auto;
                               text-align: center;
                               font-size: 80px;
                               color: white;
                               text-shadow: 15px;
                               background-color: #3C8DBC;
                               border-radius: 15px;
                               width: 70%;
                               " type="button" onclick="marcacion(this,<?php echo $_SESSION['id_operador']; ?>, 1,<?php echo $_SESSION['idhorario']; ?>)" value="Ingresar"/>
                    </div>
                <?php } else if ($_SESSION['idmarcacion_estado'] == 4) { ?>
                    <div class="row" style="text-align: center;">
                        <input style="
                               margin: 10px auto;
                               text-align: center;
                               font-size: 80px;
                               color: white;
                               text-shadow: 15px;
                               background-color: #47A447;
                               border-radius: 15px;
                               width: 70%;
                               " type="button" onclick="marcacion(this,<?php echo $_SESSION['id_operador']; ?>, 2,<?php echo $_SESSION['idhorario']; ?>)" value="Almorzar"/>
                    </div>
                <?php } else if ($_SESSION['idmarcacion_estado'] == 5) { ?>
                    <div class="row" style="text-align: center;">
                        <input style="
                               margin: 10px auto;
                               text-align: center;
                               font-size: 80px;
                               color: white;
                               text-shadow: 15px;
                               background-color: #ED9C28;
                               border-radius: 15px;
                               width: 70%;
                               " type="button" onclick="marcacion(this,<?php echo $_SESSION['id_operador']; ?>, 3,<?php echo $_SESSION['idhorario']; ?>)" value="Fin Almuerzo"
                               />
                    </div>
                <?php } else if ($_SESSION['idmarcacion_estado'] == 6) { ?>
                    <div class="row" style="text-align: center;">
                        <input style="
                               margin: 10px auto;
                               text-align: center;
                               font-size: 80px;
                               color: white;
                               text-shadow: 15px;
                               background-color: #C90000;
                               border-radius: 15px;
                               width: 70%;
                               " type="button" onclick="marcacion(this,<?php echo $_SESSION['id_operador']; ?>, 4,<?php echo $_SESSION['idhorario']; ?>)" value="Salida"/>
                    </div>
                <?php } else if ($_SESSION['idmarcacion_estado'] == 7) { ?> 
                    <div class="row" style="text-align: center;">
                        <input class="btn-lg btn-success" style="
                               margin: 10px auto;
                               text-align: center;
                               font-size: 80px;
                               color: white;
                               text-shadow: 15px;
                               border-radius: 15px;
                               " type="button" onclick="" value="Ud ya a registrado su salida."/>
                    </div>
                    <?php
                }
            } else {
                ?> 
                <div class="alert alert-error" style="font-size: 35px">
                    Está prohibido intentar marcar desde otra posición que no sea la PC asignada en la oficina, este intento de marcación vía móvil será reportado.
                </div>
            <?php } ?>
        </div>            
    </div>
    <div class = "x_panel" id="divMostrarResultados">

    </div>
    <script>
        function getTimeAJAX() {
            //GUARDAMOS EN UNA VARIABLE EL RESULTADO DE LA CONSULTA AJAX    
            var time = $.ajax({
                url: 'time.php', //indicamos la ruta donde se genera la hora
                dataType: 'text', //indicamos que es de tipo texto plano
                async: false     //ponemos el parámetro asyn a falso
            }).responseText;
            //actualizamos el div que nos mostrará la hora actual
            document.getElementById("miReloj").innerHTML = time;
        }
        //con esta funcion llamamos a la función getTimeAJAX cada segundo para actualizar el div que mostrará la hora
        var reloj1 = $('#valReloj').val();
        var seconds = 1 ;
        var relojito = setInterval(getTimeAJAX, 1000 * seconds);
        //var relojito = getTimeAJAX();
        $(document).ready(function () {
            listardetHorario(<?php echo $_SESSION['idhorario']; ?>);
        });
        asistenciaPersonalById(<?PHP echo $_SESSION['id_operador']; ?>);
    </script>
</div>

<script src="../../js/funciones.js" type="text/javascript"></script>