
<?php
//$idmarcacion = $_GET['idmarcacion'];
//include_once  '../../models/MHorario.php';
//$oHorarios1 = new MHorario();
//$objeto1 = $oHorarios1->mostrarDetalleMarcacion($idmarcacion);
//$arrayRegistros1 = $objeto1['object'];
//print_r($arrayRegistros1);
?>


<div class="col-md-12 col-sm-12 col-xs-12">
    <label class="col-md-12 col-sm-12 col-xs-12"> Tiempo Calculado</label>
    <div class="item form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Ingreso</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_ingreso">
            <input  name="h_ingreso" id="h_ingreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['begin_day']; ?>" >
        </div>
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Salida</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_egreso">
            <input   name="h_egreso" id="h_egreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['end_day']; ?>" >
        </div>
    </div> 

    <div class="item form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Break</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_break">
            <input  name="h_break" id="h_break" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['break']; ?>" >
        </div>
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Regreso</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_backbreak">
            <input   name="h_backbreak" id="h_backbreak" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['backbreak']; ?>" >
        </div>
    </div> 
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
    <label class="col-md-12 col-sm-12 col-xs-12"> Tiempo de Vista</label>
    <div class="item form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Ingreso</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_ingreso">
            <input  name="h_ingresof" id="h_ingreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['begin_dayf']; ?>" >
        </div>
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Salida</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_egreso">
            <input   name="h_egresof" id="h_egreso" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['end_dayf']; ?>" >
        </div>
    </div> 

    <div class="item form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Break</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_break">
            <input  name="h_breakf" id="h_break" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['breakf']; ?>" >
        </div>
        <label class="control-label col-md-2 col-sm-2 col-xs-2">Hora Regreso</label>
        <div class="col-md-4 col-sm-4 col-xs-4" id="h_backbreak">
            <input   name="h_backbreakf" id="h_backbreak" class="form-control col-md-7 col-sm-7 col-xs-7" data-validate-length-range="6" data-validate-words="2" placeholder="" maxlength="300" autocomplete="off" type="text"  value="<?php echo $arrayRegistros1[0]['backbreakf']; ?>" >
        </div>
    </div> 
</div>
<div class="col-md-12" title="Ingrese el motivo por el que cambiará el estado">
    <div class="form-group">
        <label>Motivo</label>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
            <textarea class="form-control" id="motivo" name="motivo" style="resize: none;"   onkeydown="quitarDis()" onkeyup="agregarDis()"></textarea> 
        </div>
    </div>
</div> 
<input type="hidden" name="idmarcacion" value="<?php echo $arrayRegistros1[0]['idmarcacion']; ?>">
<input type="hidden" name="h_ingO" value="<?php echo $arrayRegistros1[0]['begin_day']; ?>">
<input type="hidden" name="h_brkO" value="<?php echo $arrayRegistros1[0]['break']; ?>">
<input type="hidden" name="h_bbkO" value="<?php echo $arrayRegistros1[0]['backbreak']; ?>">
<input type="hidden" name="h_egrO" value="<?php echo $arrayRegistros1[0]['end_day']; ?>">
<input type="hidden" name="h_ingFO" value="<?php echo $arrayRegistros1[0]['begin_dayf']; ?>">
<input type="hidden" name="h_brkFO" value="<?php echo $arrayRegistros1[0]['breakf']; ?>">
<input type="hidden" name="h_bbkFO" value="<?php echo $arrayRegistros1[0]['backbreakf']; ?>">
<input type="hidden" name="h_egrFO" value="<?php echo $arrayRegistros1[0]['end_dayf']; ?>">