<!DOCTYPE html>
<?php
include '../config/Config.php';
$config = new Config();
//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
//session_start();
//clearstatcache();
require('includes/includes_head.php');
if ($_SESSION['dni_user'] == "") {
    header('Location: index.php');
}

//$userAgent = $_SERVER[HTTP_USER_AGENT];
//$userAgent1 = strtolower($userAgent);
//echo $userAgent1;
//$color = "";
if ($_SESSION['mobil'] == 1) {
    $newDimensionNav = "width: 1000px;";
    $newDimensionBody = 'style="' . $newDimensionNav . '"';
} else {
    $newDimensionNav = "";
    $newDimensionBody = "";
}

function funcObtener() {
    echo "ip: " . $_SERVER['HTTP_CLIENT_IP'] . "<br>";
    echo "IP Proxy: " . $_SERVER['HTTP_USER_AGENT'] . "<br>";
    echo "IP Proxy: " . $_SERVER['HTTP_X_FORWARDED_FOR'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_X_FORWARDED'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_FORWARDED_FOR'] . "<br>";
    echo "IP Access: " . $_SERVER['HTTP_FORWARDED'] . "<br>";
    echo "IP Access: " . $_SERVER['REMOTE_ADDR'] . "<br>";
}

$id_personal = "";
$color = "";
?>
<body  style="<?php echo $newDimensionNav; ?>;background-color: <?php echo $color; ?>"  class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title"><i class="fa fa-empire"></i> <span><?php echo $config->getConfig()['nombreSis']; ?></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Bienvenido(a),</span>
                            <h2><?php echo $_SESSION['nombres']; ?></h2>
                            <small class="label"><?php echo $_SESSION['nivel']; ?></small>
                        </div>
                    </div>
                    <!-- /menu prile quick info --> 
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">                            

                            <ul class="nav side-menu">
                                <h3>Configuración</h3>
                                <li>
                                    <a onclick="index('<?php echo $config->getConfig()['urlBase']; ?>')"><i class="fa fa-home"></i> Principal</a>
                                </li>
                                <?php if ($_SESSION['id_nivel'] == '1' || $_SESSION['id_nivel'] == '2') { ?>
                                    <li><a><i class="fa fa-user"></i>Operadores <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a onclick="listadoOperadores();">Lista de Operadores</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <?php
                                }
                                if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
                                    ?>      
                                    <h3> Administracion y Gestión </h3>
                                    <li>
                                        <a><i class="fa fa-clock-o"></i> Marcación <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none"> 
                                            <div class="col-md-1"></div><label class="label"><i class="fa fa-tags"></i> Gestión</label> 
                                            <li><a onclick="listHorasTrabajadasxMes()">Asistencia Mensual</a>
                                            </li> 
                                            <li><a onclick="listHorasTrabajadasxDia()">Asistencia Diaria</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-gears"></i> Mantenimientos <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none"> 
                                            <div class="col-md-1"></div><label class="label"><i class="fa fa-table"></i> Tablas</label> 
                                            <li><a onclick="listHorarios()">Horarios</a>
                                            </li> 
                                        </ul>
                                    </li>
                                    <?php
                                }
                                if ($_SESSION['id_nivel'] == '1') {
                                    # code...
                                    ?>
                                    <li><a><i class="fa fa-bug"></i> Configuración<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu" style="display: none">
                                            <li><a href="<?php echo $config->getConfig()['urlBase']; ?>php/views/ejemplo.php?segun=1&hora=01:00:00"> Ejemplos</a>
                                            </li>
                                            <li><a href="<?php echo $config->getConfig()['urlBase']; ?>php/config/tarea1.php">Restablecer Día</a>
                                            </li>
                                            <li><a href="<?php echo $config->getConfig()['urlBase']; ?>php/config/tarea2.php">Restablecer Noche</a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>

                                <li>
                                    <a><i class="fa fa-area-chart"></i> Reportes <span class="label label-warning pull-right">New</span></a>
                                    <ul class="nav child_menu" style="display: none"> 
                                        <div class="col-md-1"></div><label class="label"><i class="fa fa-bar-chart"></i> Diario</label> 
                                        <?php
                                        if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) {
                                            ?>   
                                            <li>
                                                <a onclick="verReporte1()">Resumen mes Actual</a>
                                            </li>  
                                        <?php } ?>
                                        <li>
                                            <a onclick="verReporte2()">Reportes de Asistencia &nbsp;&nbsp;&nbsp;<i class="fa fa-check" style="background-color: #F0AD4E;text-align: center;border-radius: 25px;"></i> </a>
                                        </li>  
                                    </ul>
                                </li>
                            </ul>

                            <ul class="nav side-menu">

                            </ul>                            
                        </div>
                        <div class="menu_section">

                        </div>
                    </div>
                    <!-- /sidebar menu -->
                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a href="" data-toggle="tooltip" data-placement="top" title="Salir" onclick="salir('<?php echo $config->getConfig()['urlBase']; ?>')">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right"> 
                            <li class="">
                                <!--<label class="label-primary center-block">Hola</label>-->
                                <!--<h4 class="label-primary">Hola</h4>-->
                            </li>
                            <li class=""> 
                                <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/Fotos/<?php echo $_SESSION['foto']; ?>"><?php echo $_SESSION['nombres']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <?php $id_personal = $_SESSION['id_operador']; ?>
                                        <a onclick="listarDetOperador(
                                        <?php echo $_SESSION['id_operador']; ?>,
                                        <?php echo $_SESSION['idarea']; ?>,
                                        <?php echo $_SESSION['idturno']; ?>,
                                        <?php echo $_SESSION['id_nivel']; ?>,
                                        <?php echo $_SESSION['idestado']; ?>,
                                        <?php echo $_SESSION['idmarcacion_estado']; ?>,
                                        <?php echo $_SESSION['idhorario']; ?>
                                                )"> 
                                            <span class="badge bg-blue pull-right"><i class="fa fa-user pull-right"></i></span>Perfil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span><?php echo $_SESSION['area']; ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="doc/ayuda.php" target="_blank" onclick="window.open(this.href, this.target, 'width=1100,height=700,top=0,left=700,resizable=no,toolbar=no,location=yes,status=no,menubar=no,scrollbars=yes');
                                                return false;">
                                            <span class="badge bg-orange pull-right"><i class="fa fa-question-circle pull-right"></i></span>Ayuda
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="salir('<?php echo $config->getConfig()['urlBase']; ?>')">
                                            <span class="badge bg-red pull-right"><i class="fa fa-sign-out pull-right"></i></span> Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li> 
                            <li class="">
                                <a href="#">
                                    <!--<i class="fa fa-power-off"></i>-->
                                    <?php
                                    $localtime = localtime();
                                    $localtime_assoc = localtime(time(), true);
                                    $dia = $localtime['3'];
                                    $mes = $localtime['4'] + 1;
                                    $año = $localtime['5'] + 1900;
                                    echo '<b>Fecha: ' . $dia . ' de ' . $config->getNombreMes() . ' del ' . $año . '</b>&nbsp;&nbsp;  ';
                                    ?>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->
            <div class="right_col" role="main">

                <div class="row"> 
                    <div class="content" id="content" >

                    </div> 
                    <script>
                        listMarcacion();
                        //asistenciaPersonal(<?PHP echo $_SESSION['id_operador']; ?>);
                        //listardetHorario(<?php echo $_SESSION['idturno']; ?>);
                    </script>

                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer style="margin-top: 1000px">
                    <div 
                        <strong style="">Copyright &copy; <?php echo $config->getConfig()['anio'] ?> <a href="<?php echo $config->getConfig()['webEmpresa'] ?>"><?php echo $config->getConfig()['empresa'] ?></a>.</strong> All rights reserved.

                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>
        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
    </div>
    <?php
    require ('includes/includes_foot.php');
    ?>
</body>

</html>

<?php
//require 'includes/footer.php';
