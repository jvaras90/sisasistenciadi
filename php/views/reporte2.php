<?php
//var_dump($arrayRegistros);
include '../config/CConexion.php';
$config = new Config();
$url = "";
?>  

<script src="../../js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="../../js/jquery-ui.js" type="text/javascript"></script>
<link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script  type="text/javascript">
    jQuery.datepicker.regional['eu'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['eu']);
    $(function () {
        $("#dia").datepicker();
        $("#fecDesde").datepicker();
        $("#fecHasta").datepicker();
        $("#gesDesde").datepicker();
        $("#gesHasta").datepicker();
    });
</script>
<div class = "x_title">
    <h1 style = "text-align: center;">Reporte General de Asistencia </h1> 
    <div class = "clearfix"></div>
</div>
<div class="x_content">

 
       


    <div title="FILTROS">
        <form id="formFiltrosReporte1" name="formFiltrosReporte1" method="POST">
            <table style="background-color: #f7f7f7;text-align: center;margin: auto">
                <tr>
                    <td colspan="4"  style="text-align: center;padding: 1px; margin: 1px;"><strong>Fecha</strong></td>
                    <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>Turno</strong></td>
                    <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong>Sede</strong></td>
                    <td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong></strong></td> 
                </tr>
                <tr>
                    <td style="background-color: #a8e3d7; padding: 5px; margin: 2px;"><b>Desde:</b> </td>
                    <td style="background-color: #a8e3d7; padding: 5px; margin: 2px;">
                        <input class="form-control" name="fecDesde" type="text" id="fecDesde"  size="8" value="<?php
                        if (isset($_POST["fecDesde"])) {
                            if ($_POST['fecDesde'] == '') {
                                echo date('Y-m') . "-01";
                                $url .= "?fecDesde=" . date('Y-m') . "-01";
                            } else {
                                echo $_POST["fecDesde"];
                                $url .= "?fecDesde=" . $_POST["fecDesde"];
                            }
                        } else {
                            echo date('Y-m') . "-01";
                        }
                        ?>" class="txtDate startDate"> </td>
                    <td style="background-color: #a8e3d7; padding: 5px; margin: 1px;"><b>Hasta: </b></td>
                    <td style="background-color: #a8e3d7; padding: 5px; margin: 1px;">
                        <input class="form-control" name="fecHasta" type="text" id="fecHasta"  size="8" value="<?php
                        if (isset($_POST["fecHasta"])) {
                            if ($_POST['fecHasta'] == '') {
                                echo date('Y-m-d');
                                $url .= "&fecHasta=" . date('Y-m-d');
                            } else {
                                echo $_POST["fecHasta"];
                                $url .= "&fecHasta=" . $_POST["fecHasta"];
                            }
                        } else {
                            echo date('Y-m-d');
                        }
                        ?>" class="txtDate startDate" > 
                    </td>

                    <td style="background-color: #9abcc3; padding: 5px; margin: 1px;">
                        <select class="form-control" name="idturno" id="idturno">
                            <option value="">Turno</option>
                            <option value="1" <?php
                            if (isset($_POST["idturno"]) && $_POST["idturno"] == "1") {
                                echo "selected";
                            }
                            ?> >Dia</option>
                            <option value="2" <?php
                            if (isset($_POST["idturno"]) && $_POST["idturno"] == "2") {
                                echo "selected";
                            }
                            ?> >Noche</option>                    
                        </select> 
                    </td>
                    <td style="background-color: #a8e3d7; padding: 5px; margin: 1px;">
                        <select class="form-control" name="idsede" id="idsede">
                            <option value="">Sede</option>
                            <option value="1" <?php
                            if (isset($_POST["idsede"]) && $_POST["idsede"] == "1") {
                                echo "selected";
                            }
                            ?> >Lima</option>
                            <option value="2" <?php
                            if (isset($_POST["idsede"]) && $_POST["idsede"] == "2") {
                                echo "selected";
                            }
                            ?> >Chorrillos</option>                    
                        </select> 
                    </td>                    
                    <td style="background-color: whitesmoke; padding: 5px; margin: 1px;"> 
                        <input type="hidden" name="controlador" id="controlador" value="reporte">
                        <input type="hidden" name="funcion" id="funcion" value="verReporte1"> 
                        <input type="hidden" name="generarReporte" id="generarReporte" value="True"> 
                        <input id="enlace" type="button" class="btn btn-danger" value="Generar">
                        <!--<a class="btn btn-danger"> <i class="fa fa-file-excel-o"></i> Generar</a>-->
                    </td>
                </tr>
            </table> <br>
            <?php
            if (isset($_POST['idturno']) && $_POST['idturno'] != '') {
                $url .="&idturno=" . $_POST['idturno'];
            }
            if (isset($_POST['idsede']) && $_POST['idsede'] != '') {
                $url .="&idsede=" . $_POST['idsede'];
            }
            if (isset($_POST['generarReporte']) && $_POST['generarReporte'] != '') {
                $url .="&generar=" . $_POST['generarReporte'];
            }
            //echo $url;
            ?>

        </form>

    </div>
    <div id="contenido" >

    </div>
    <!--<iframe src="http://server-di:8080/sisasistenciadi/php/views/reporte1.php<?php echo $url; ?>" height="1000" width="100%"></iframe>-->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#enlace').click(function () {

                //Añadimos la imagen de carga en el contenedor
                $('#contenido').html('<div style="margin: auto;padding: 1px;text-align: center;"><h2><b>Cargando Datos.. Un momento porfavor</b></h2><img src="images/loading.gif"/></div>');

                //var page = $(this).attr('data');
                var idturno = $('#idturno').val();
                var idsede = $('#idsede').val();
                var fecDesde = $('#fecDesde').val();
                var fecHasta = $('#fecHasta').val();
                var generarReporte = $('#generarReporte').val();
                $.ajax({
                    type: "GET",
                    url: "reporte1.php",
                    data: "&idturno=" + idturno + "&idsede=" + idsede + "&fecDesde=" + fecDesde + "&fecHasta=" + fecHasta + "&generar=" + generarReporte,
                    success: function (data) {
                        //Cargamos finalmente el contenido deseado
                        $('#contenido').fadeIn(1000).html(data);
                    }
                });
            });
        });
    </script>
</div>

