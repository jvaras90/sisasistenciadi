<?php
//var_dump($arrayRegistros);
include '../config/CConexion.php';
$config = new Config();
$url = "";
if ($_SESSION['id_nivel'] == 1) {
    $colSpan = 7;
} else {
    $colSpan = 5;
}
?>  

<script src="../../js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="../../js/jquery-ui.js" type="text/javascript"></script>
<link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script  type="text/javascript">
    jQuery.datepicker.regional['eu'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['eu']);
    $(function () {
        $("#dia").datepicker();
        $("#fecDesde").datepicker();
        $("#fecHasta").datepicker();
        $("#gesDesde").datepicker();
        $("#gesHasta").datepicker();
    });
</script>
<div class = "x_title">


    <div title="FILTROS">
        <form id="formFiltrosReporte1" name="formFiltrosReporte1" method="POST">
            <table style="background-color: #f7f7f7;text-align: center;margin: auto">
                <tr>
                    <td colspan="<?php echo $colSpan; ?>"  style="text-align: center;padding: 1px; margin: 1px;font-size: 25px;"><strong>Registros de Asistencia</strong></td> 
                    <!--<td colspan="1"  style="text-align: center;padding: 1px; margin: 1px;"><strong></strong></td>--> 
                </tr>
                <tr>
                    <?php if ($_SESSION['id_nivel'] == 1 || $_SESSION['id_nivel'] == 2) { ?>
                        <td style="background-color: #9abcc3; padding: 5px; margin: 1px;">Operador</td>
                        <td style="background-color: #9abcc3; padding: 5px; margin: 1px;">
                            <?php include '../views/dropDownList/listaOperadores.php'; ?> 
                        </td>
                    <?php } else { ?>
                    <input type="hidden" name="idoperador" id="idoperador" value="<?php echo $_SESSION['id_operador']; ?>">
                <?php } ?>
                <td style="background-color: #a8e3d7; padding: 3px; margin: 1px;"><b>Desde:</b> </td>
                <td style="background-color: #a8e3d7; padding: 3px; margin: 1px;">
                    <input class="form-control"  name="fecDesde" type="text" id="fecDesde"  size="8" value="<?php
                    if (isset($_POST["fecDesde"])) {
                        if ($_POST['fecDesde'] == '') {
                            echo date('Y-m') . "-01";
                            $url .= "?fecDesde=" . date('Y-m') . "-01";
                        } else {
                            echo $_POST["fecDesde"];
                            $url .= "?fecDesde=" . $_POST["fecDesde"];
                        }
                    } else {
                        echo date('Y-m') . "-01";
                    }
                    ?>" class="txtDate startDate"> </td>
                <td style="background-color: #a8e3d7; padding: 3px; margin: 1px;"><b>Hasta: </b></td>
                <td style="background-color: #a8e3d7; padding: 3px; margin: 1px;">
                    <input class="form-control"  name="fecHasta" type="text" id="fecHasta"  size="8" value="<?php
                    if (isset($_POST["fecHasta"])) {
                        if ($_POST['fecHasta'] == '') {
                            echo date('Y-m-d');
                            $url .= "&fecHasta=" . date('Y-m-d');
                        } else {
                            echo $_POST["fecHasta"];
                            $url .= "&fecHasta=" . $_POST["fecHasta"];
                        }
                    } else {
                        echo date('Y-m-d');
                    }
                    ?>" class="txtDate startDate" > 
                </td>                 
                <td style="background-color: whitesmoke; padding: 5px; margin: 1px;">                         
                    <input type="hidden" name="controlador" id="controlador" value="reporte">
                    <input type="hidden" name="funcion" id="funcion" value="verReporte1"> 
                    <input type="hidden" name="generarReporte" id="generarReporte" value="True"> 
                    <input id="enlace" type="button" class="btn btn-danger" value="Generar">
                </td>
                </tr>
            </table> <br>
        </form>
    </div>
    <div class = "clearfix"></div>
</div>
<div class="x_content">
    <div id="contenido" >
    </div>    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#enlace').click(function () {
                //Añadimos la imagen de carga en el contenedor
                $('#contenido').html('<div style="margin: auto;padding: 1px;text-align: center;"><h2><b>Cargando Datos.. Un momento porfavor</b></h2><img src="images/loading.gif"/></div>');
                var fecDesde = $('#fecDesde').val();
                var fecHasta = $('#fecHasta').val();
                var idoperador = $('#idoperador').val();
                var generarReporte = $('#generarReporte').val();
                var idoperador = $('#idoperador').val();
                var dato = "&fecDesde=" + fecDesde + "&fecHasta=" + fecHasta + "&idoperador=" + idoperador;
                $.ajax({
                    url: 'index.php',
                    type: 'POST',
                    async: false,
                    data: 'controlador=reporte&funcion=mostrarAsistenciaPorRangoFec' + dato,
                    success: function (datos) {
                        $('#contenido').fadeIn(1000).html(datos);
                        //$(".content").html(datos);
                        //listarTurnoOpe();
                    }
                });
            });
        });
    </script>
</div>

