<!DOCTYPE html>
<html>
    <?php
    include '../config/CConexion.php';
    $cn = new CConexion();
    $cn->CConectarse();
    //include '../config/Config.php';
    $config = new Config();
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> <?php echo $config->getConfig()['nombreSis'] . " | " . $config->getConfig()['empresa'] ?></title>
        <link href="images/logo.ico" rel="shortcut icon" />
        <!-- Bootstrap core CSS -->

        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $config->getConfig()['urlBase'] ?>fonts/css/font-awesome.min.css" rel="stylesheet"> 
        <!-- Custom styling plus plugins -->
        <link href="<?php echo $config->getConfig()['urlBase'] ?>css/custom.css" rel="stylesheet"><link href="<?php echo $config->getConfig()['urlBase'] ?>css/icheck/flat/green.css" rel="stylesheet" />

        <script src="<?php echo $config->getConfig()['urlBase'] ?>js/js.js" type="text/javascript"></script>
        <script src="<?php echo $config->getConfig()['urlBase'] ?>js/jquery.min.js"></script>
        <style>
            .errorImput1{ 
                border: 1px solid #e80c4d;
                font-weight: bold;
                font-size: 1.1em;
            }
        </style>
        <script src="../../js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="../../js/jquery-ui.js" type="text/javascript"></script>
        <link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    </head>
    <body style="background-color: whitesmoke">
    
       
            <?php require_once './tables/tableReport2.php'; ?>
        
        <div style="height: 200px;">&nbsp;</div>



    </body>


    <script language="javascript">
        $(document).ready(function () {
            $("#botonExcel").click(function (event) {
                $("#datos_a_enviar").val($("<div>").append($("#Exportar_a_Excel").eq(0).clone()).html());
                $("#FormularioExportacion").submit();
            });
        });
    </script>
    <style type="text/css">
        #botonExcel{cursor:pointer;}
    </style>
</html>