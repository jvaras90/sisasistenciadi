<?php
//var_dump($arrayRegistros);
$config = new Config();
?> 
<div class = "x_title">
    <h1 style = "text-align: center;">Listado de Asistencia <?php echo $config->getNombreDiaSemana() . " " . date("d") . " de " . $config->getNombreMes() ; ?> </h1> 
    <div class = "clearfix"></div>
</div>
<div class="x_content">
    <table id="example" class="table table-striped responsive-utilities jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th>
                    <input type="checkbox" id="check-all" class="flat">
                </th> 
                <th class="column-title">Nombres</th>
                <th class="column-title">DNI</th>
                <th class="column-title">Ingreso</th>
                <th class="column-title">Break</th>
                <th class="column-title">BackBreak</th> 
                <th class="column-title">Salida</th> 
                <th class="column-title">Marcación</th> 
                <th class="column-title">H. Tardanza</th> 
                <th class="column-title">H. Break</th> 
                <th class="column-title">H. Trabajadas</th> 
                <th class="column-title">Detalle</th>
                <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                </th>
                <th class="bulk-actions" colspan="7">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($arrayRegistros as $key => $value) {
                ?> 
                <tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td> 
                    <td class="">
                        <?php
                        echo utf8_decode($value['nombres']);
                        if ($_SESSION['id_nivel'] == 1) {
                            echo " <b>(id: " . $value['idoperador'] . ")</b>";
                        }
                        echo "<br>Sede: " . $value['sede'];
                        ?>
                    </td>
                    <td class=""><?php echo $value['dni']; ?></td>
                    <td class=""><?php
                        echo $value['begin_dayf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['begin_day'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class=""><?php
                        echo $value['breakf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['break'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class=""><?php
                        echo $value['backbreakf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['backbreak'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class="" <?php
                    if ($value['fecha_marcada'] < $config->getFechaCortaActual() && $value['end_dayf'] == "00:00:00" && $value['begin_day'] != '00:00:00') {
                        echo 'style="background-color: #ff8c8c;color:red;" title="Olvidó marcar su salida, esto probablemente se vea afectado en el resultado de Horas Trabajadas. Debe enviar un correo a las personas encargadas para modificar el registro."';
                    }
                    ?> ><?php
                            echo $value['end_dayf'];
                            if ($_SESSION['id_nivel'] == 1) {
                                echo "<br><span style='color:blue'>" . $value['end_day'] . "</span>";
                            }
                            ?>
                    </td>
                    <td class="" title="Referencial a la hora de llegada del personal"><?php
                        //echo $oHorarios->getHorasSegundo($value['tardanzaf']);
                        if ($value['begin_day'] == "00:00:00" && $value['end_day'] == "00:00:00" && $value['break'] == "00:00:00" && $value['backbreak'] == "00:00:00") {
                            echo "<span style='color:red'><b>Ausente</b></span>";
                        } else {
                            if ($value['tardanzaf'] == '00:00:00' || $value['tardanzaf'] < '00:00:00') {
                                echo " Temprano";
                            } else {
                                echo "<span style='color:red'>Tarde</span>";
                            }
                        }
                        if ($value['idmarcacion_est'] == 4) {
                            $btn = "primary";
                        } else if ($value['idmarcacion_est'] == 5) {
                            $btn = "success";
                        } else if ($value['idmarcacion_est'] == 6) {
                            $btn = "warning";
                        } else if ($value['idmarcacion_est'] == 7 || $value['idmarcacion_est'] == 3) {
                            $btn = "danger";
                        }
                        echo "<br><small class='label label-$btn'><b>" . $value['estMarcacion'] . "</b></small>";
                        ?>
                    </td>                    
                    <td class="" title="Rango de Ingreso Marcado en el sistema
                        Ingreso menor a 8:14       -> Marcación en Sistema 8:00am
                        Ingreso entre 8:15 a 8:44 -> Marcación en Sistema 8:30am
                        Ingreso entre 8:45 a 9:14 -> Marcación en Sistema 9:00am
                        Ingreso después de 9:15  -> Marcación en tiempo real"><?php
                            if ($value['tardanzaf'] == '00:00:00' || $value['tardanzaf'] < '00:00:00') {
                                echo "00:00:00";
                            } else {
                                echo ($value['tardanzaf']);
                            }
                            if ($_SESSION['id_nivel'] == 1) {
                                echo "<br><span style='color:blue'>" . ($value['tardanza']) . "</span>";
                            }
                            ?>
                    </td>
                    <td class=""><?php
                        echo ($value['almuerzo']);
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . ($value['almuerzo']) . "</span>";
                        }
                        ?>
                    </td>
                    <td class="" ><!--Horas Trabajadas-->
                        <?php
                        if ($value['horas_trabajo'] < "00:00:00") {
                            echo "00:00:00";
                        } else {
                            echo $value['horas_trabajo'];
                        }
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . ($value['asistencia']) . "</span>";
                        }
                        ?>
                    </td>
                    <td class="" style="text-align: center;"> 
                        <div class="col-md-12 col-sm-12 col-xs-12"> 
                            <div class="col-md-6 col-sm-6 col-xs-12"> 
                                <a onclick="listarDetOperador(
                                <?php echo $value['idoperador']; ?>,
                                <?php echo $value['idarea']; ?>,
                                <?php echo $value['idturno']; ?>,
                                <?php echo $value['idtipo_usuario']; ?>,
                                <?php echo $value['idestado']; ?>,
                                <?php echo $value['idmarcacion_est']; ?>,
                                <?php echo $value['idhorario']; ?>
                                            )" class="btn-sm btn-round btn-primary " title="Ver detalle"><i class="fa fa-pencil"></i></a>
                                
                                <div class="clearfix"></div>
                            </div><br><br>
                            <?php if ($_SESSION['idarea'] == '2') { ?>
                                <div class="col-md-6 col-sm-6 col-xs-12"> 
                                    <a href="#modalCambiarEstado" data-toggle="modal" class="btn-sm btn-round  btn-danger" onclick="mostrar('<?php echo $value['idmarcacion']; ?>')"title="Modificar Horarios"><i class="fa fa-pencil"></i></a>                                         
                                    <div class="clearfix"></div>
                                </div>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div> 

<!-- ############################################ MODAL CAMBIAR ESTADO ############################################ -->
<div class="modal fade" id="modalCambiarEstado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <script type="text/javascript">
        function quitarDis() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length > 4 && idestado != 0) {
                $('#cambiarEstado').removeAttr('disabled');
            }
        }
        function agregarDis() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length < 4 || idestado == 0) {
                $('#cambiarEstado').attr('disabled', 'disabled');
            }
        }
        function valSelectCambio() {
            var motivo = $('#motivo').val();
            var idestado = $('#idestado2').val();
            if (motivo.length < 4 || idestado == 0) {
                $('#cambiarEstado').attr('disabled', 'disabled');
            } else if (motivo.length > 4 && idestado != 0) {
                $('#cambiarEstado').removeAttr('disabled');
            }
        }
        function mostrar(id) {
            $(document).ready(function () {
                $("#result").hide("slow");
                $("#cargar_reporte").show("slow");

                $.ajax({
                    url: 'index.php',
                    type: 'POST',
                    async: false,
                    data: 'controlador=principal&funcion=mostrarDetalleMarcacion&id=' + id,
                    success: function (datos) {
                        //$(".content").html(datos);
                        $("#editar_resul").show("slow");
                        $("#cargar_reporte").hide("slow");
                        $("#editar_resul").html(datos);
                    }
                });
                // $("#editar_resul").load("modal/modalActMarcacion.php?idmarcacion=" + id, "&idestado=0", function () {
                //   $("#editar_resul").show("slow");
                // $("#cargar_reporte").hide("slow");
                //});
            });
        }
    </script> 
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form"  method="post" name="formModificarMarcacion" id="formModificarMarcacion">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align: center;">Modificar Marcación <small><b>(Exclusivamente Root)</b></small></h4>
                </div>
                <div class="modal-body">
                    <center>
                        <div id="cargar_reporte" > 
                            <div class="a-center">
                                <img src="<?php echo $config->getConfig()['urlBase']; ?>img/loading.gif"/> 
                            </div>
                            <label>Espere!!! Cargando datos...</label>
                        </div>
                    </center>
                    <div class="panel-body" id="editar_resul" >
                    </div> 
                </div>
                <div class="clearfix"></div>                                                            
                <div class="modal-footer">
                    <div style="float: right;">
                        <button type="submit" onclick="actualizarMarcacion();" id="cambiarEstado" name="cambiarEstado" class="btn btn-primary" disabled form="formCambiarEstado" >Guardar</button>
                    </div>
                    <div style="float: right;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div> 

                    <input type="hidden" name="idpersona" value="<?php echo $_SESSION['id_operador']; ?>"/>
                    <input type="hidden" name="fecha_mod" value="<?php echo date("Y-m-d"); ?>"/>
                    <input type="hidden" name="hora_mod" value="<?php echo date("H:i:s"); ?>"/>
                    <input type="hidden" name="form" value="formModificarMarcacion"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- icheck -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/icheck/icheck.min.js"></script> 
<!-- Datatables -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/datatables/js/jquery.dataTables.js"></script> 
<script>

</script>
<script>
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Buscar:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': <?php echo "10"; ?>,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>