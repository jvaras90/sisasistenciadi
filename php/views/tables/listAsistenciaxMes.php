<?php
//var_dump($arrayRegistros);
$config = new Config();
?> 
<div class = "x_title">
    <h4 style = "text-align: center;">Listado de Asistencia Mes: <?php echo "<b>".$config->getNombreMes()."</b>"; ?></h4> 
    <div class = "clearfix"></div>
</div>
<div class="x_content">
    <table id="example" class="table table-striped responsive-utilities jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th>
                    <input type="checkbox" id="check-all" class="flat">
                </th> 
                <th class="column-title">Nombres</th>
                <th class="column-title">DNI</th>
                <th class="column-title">Tardanza</th>
                <th class="column-title">Break</th>
                <th class="column-title">Horas Trabajadas</th> 
                <th class="column-title">Detalle</th>    
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($arrayRegistros as $key => $value) {
                ?> 
                <tr class="even pointer">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td> 
                    <td class="">
                        <?php
                        echo utf8_decode($value['nombres']);
                        if ($_SESSION['id_nivel'] == 1) {
                            echo " <b>(id: " . $value['idoperador'] . ")</b>";
                        }
                        echo "<br>Sede: " . $value['sede'];
                        ?>
                    </td>
                    <td class=""><?php echo $value['dni']; ?></td>
                    <td class=""><?php
                        if ($value['tardanza'] < '00:00:00') {
                            echo "00:00:00";
                        } else {
                            echo ($value['tardanza']);
                        }
                        ?></td>
                    <td class=""><?php echo ($value['almuerzo']); ?></td>
                    <td class=""><?php
                        if ($value['total_horas'] < '00:00:00') {
                            echo "00:00:00";
                        } else {
                            echo ($value['total_horas']);
                        }
                        ?>
                    </td> 
                    <td class="last"> 
                        <a onclick="listarDetOperador(<?php echo $value['idoperador']; ?>,<?php echo $value['idarea']; ?>,<?php echo $value['idturno']; ?>,<?php echo $value['idtipo_usuario']; ?>)" class="btn-sm btn-round btn-primary " title="Ver detalle"><i class="fa fa-pencil"></i></a>
                    <td> 
                </tr>
            <?php } ?>

        </tbody>

    </table>
</div> 



<!-- icheck -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/icheck/icheck.min.js"></script> 
<!-- Datatables -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/datatables/js/jquery.dataTables.js"></script> 
<script>

</script>
<script>
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Buscar:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': <?php echo "10"; ?>,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>