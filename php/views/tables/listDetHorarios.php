<?php
//echo "id: $id | desc : $desc <br>";
//print_r($arrayHorario);
$descripcion = $arrayHorario[0]['descripcion'];
$idhorario = $arrayHorario[0]['idhorario'];
$idturno = $arrayHorario[0]['idturno'];
$aDia[] = "";
$config = new Config();
//print_r($aDia);
?>
<!-- select2 -->
<link href="<?php echo $config->getConfig()['urlBase']; ?>css/select/select2.min.css" rel="stylesheet">
<div class="x_panel">
    <div class="x_title">
        <div class="col-md-10 col-sm-10 col-xs-12">
            <!--<input class="errorImput1" type="text" id="dni" name="dni" placeholder="DNI" required>-->
            <h2 style="text-align: center">Horas de Marcación de <b>" <?php
                    echo utf8_decode($descripcion);
                    if ($_SESSION['id_nivel'] == 1) {
                        echo "<b> (id: $idhorario) </b>";
                    }
                    ?>"</b></h2>
        </div>  
        <div class="clearfix"></div> 

    </div>
    <div class="x_content">

        <form method="POST" id="form_horarios_horas" name="form_horarios_horas" onsubmit="actualizarRegHorarios(<?php echo "$idhorario,$idturno"; ?>)">
            <div class="col-md-12 col-sm-12 col-xs-12" id="divAgregarHoras">
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                    <div class="col-md-2 col-sm-2 col-xs-12"> 
                        <label>Descripción</label>  
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-12 "> 
                        <input class="form-control" type="text" name="descripcion" id="descripcion" value="<?php echo utf8_decode($descripcion) ?>" > 
                        <input type="hidden" name="idhorario" id="idhorario" value="<?php echo $idhorario ?>">  
                    </div>
                </div>
                <div class="clearfix"></div> <br>
                <div class="col-md-12 col-sm-12 col-xs-12"> 

                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Turno</label>
                    <div class="col-md-10 col-sm-10 col-xs-10" id="divOpe_turno">
                    </div>
                </div>
                <div class="clearfix"></div> <br>
                <div class="col-md-12 col-sm-12 col-xs-12"> 
                    <div class="col-md-2 col-sm-2 col-xs-12"> 
                        <label>Seleccione Dias</label>  
                    </div>
                    <div id="selectMultiple"  class="col-md-10 col-sm-10 col-xs-12 " title="Seleccione los dias que contendrán el horario">
                        <select class="select2_multiple form-control" multiple="multiple" name='dia[]' id='dia'>
                            <option value="1" <?php
                            if (in_array('1', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Lunes</option>
                            <option value="2" <?php
                            if (in_array('2', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Martes</option>
                            <option value="3" <?php
                            if (in_array('3', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Miercoles</option>
                            <option value="4" <?php
                            if (in_array('4', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Jueves</option>
                            <option value="5" <?php
                            if (in_array('5', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Viernes</option>
                            <option value="6" <?php
                            if (in_array('6', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Sábado</option>
                            <option value="7" <?php
                            if (in_array('7', $aDia)) {
                                echo " selected ";
                            }
                            ?>>Domingo</option>
                        </select>                                    
                    </div> 
                </div>
                <div class="clearfix"></div>  <br>
                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
                    <a href="#modalNuevaHora" class="btn btn-success" data-toggle="modal"><i class="fa fa-clock-o"></i> Establecer Rango de Horas</a>
                </div>
                <div class="clearfix"></div> 
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" id="divAgregarHoras">
                <table id="example12" class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Dia</th>
                            <th class="column-title">Ingreso</th>
                            <th class="column-title">Break</th>
                            <th class="column-title">Regreso B.</th>
                            <th class="column-title">Salida</th>
                            <th class="column-title">Detalle</th>
                            <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                            </th>
                            <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($arrayHorario as $key => $value) {
                            ?> 
                            <tr id="<?php echo $value['idhorario_marc']; ?>" class="impreso">
                                <td class=""><input type="hidden" name="numDia<?php echo $value['dia']; ?>" value="<?php echo $value['dia']; ?>"/><?php echo $config->getConfig()['nomDias'][$value['dia']]; ?></td>
                                <td class=""><input type="hidden" name="ingreso<?php echo $value['dia']; ?>" value="<?php echo $value['h_ingreso']; ?>"/><?php echo $value['h_ingreso']; ?></td>
                                <td class=""><input type="hidden" name="break<?php echo $value['dia']; ?>" value="<?php echo $value['h_break']; ?>"/><?php echo $value['h_break']; ?></td>
                                <td class=""><input type="hidden" name="bkbreak<?php echo $value['dia']; ?>" value="<?php echo $value['h_backbreak']; ?>"/><?php echo $value['h_backbreak']; ?></td>
                                <td class=""><input type="hidden" name="salida<?php echo $value['dia']; ?>" value="<?php echo $value['h_salida']; ?>"/><?php echo $value['h_salida']; ?></td>
                                <td class="last">  
                                    <a class="btn-sm btn-round  btn-danger" onclick="eliminarHora('<?php echo $value['idhorario_marc']; ?>');" title="Quitar éste rango de horas y día del horario" style="width: 100%"><i class="fa fa-minus"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table> 
            </div> 
            <div class="clearfix"></div> 
            <div class="panel-footer" style="text-align: center;"> 
                <button class="btn btn-primary" type="submit" >Guardar</button>
                <!--<a class="btn btn-primary">Guardar</a>-->
            </div>
        </form>
    </div>
</div>



<!-- ############################################ MODAL NUEVO AGENTE ############################################ -->

<div class="modal fade" id="modalNuevaHora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="formRegistroHorario" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo Horario <i style="color: red" id="messageError"></i> </h4>
                </div>
                <div class="modal-body">   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Ingreso</label>
                            <div class="input-group" id="descripcionG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input value="08:00:00" type="text" id="h_ingreso" name="h_ingreso" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>       
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Break</label>
                            <div class="input-group" id="descripcionG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input value="12:00:00" type="text" id="h_break" name="h_break" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Regreso Break</label>
                            <div class="input-group" id="descripcionG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input value="13:00:00" type="text" id="h_backbreak" name="h_backbreak" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Salida</label>
                            <div class="input-group" id="descripcionG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input value="18:00:00" type="text" id="h_salida" name="h_salida" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                    </div>               
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="button" onclick="agregarSemana();" class="btn btn-primary">Agregar</button>                    
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
<!-- icheck -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/icheck/icheck.min.js"></script> 
<!-- Datatables -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/datatables/js/jquery.dataTables.js"></script> 
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/select/select2.full.js"></script>
<script>

</script>
<script>
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    $("#dia").select2({});
    $("#dia").select2({
        maximumSelectionLength: <?php echo 7; ?>,
        placeholder: "Seleccione Máximo " + <?php echo 7; ?> + " Dias",
        allowClear: true
    });
</script>

<script>
    function AgregarHoras() {
        $('#divAgregarHoras').removeClass();
        $('#divAgregarHoras').addClass('col-md-12 col-sm-12 col-xs-12');
    }
    function agregarSemana() {
        /*$("#example12 tbody tr").each(function () {
         this.parentNode.removeChild(this);
         });*/

        var valDia1 = "" + $("#dia").val() + "";
        var cant_dia = $('#dia option:selected').size();
        var cadDias = valDia1,
                separador = ",", // un espacio en blanco
                limite = cant_dia,
                aDias = cadDias.split(separador, limite);
        aDias.forEach(addFilas);
    }
    function addFilas(cn, indice, array) {
        var cad = "", nomDia = "";
        var h_ingreso = $('#h_ingreso').val(),
                h_break = $('#h_break').val(),
                h_backbreak = $('#h_backbreak').val(),
                h_salida = $('#h_salida').val();
        switch (cn) {
            case '1' :
                nomDia = 'Lunes';
                break;
            case '2' :
                nomDia = 'Martes';
                break;
            case '3' :
                nomDia = 'Miércoles';
                break;
            case '4' :
                nomDia = 'Jueves';
                break;
            case '5' :
                nomDia = 'Viernes';
                break;
            case '6' :
                nomDia = 'Sábado';
                break;
            case '7' :
                nomDia = 'Domingo';
                break;
            default :
                nomDia = 'no existe';
        }
        cad += '<tr id="' + cn + '" class="impreso"> \n\
        <td><input type="hidden" name="numDia' + cn + '" value="' + cn + '"/>' + nomDia + '</td> \n\
    <td><input type="hidden" name="ingreso' + cn + '" value="' + h_ingreso + '"/>' + h_ingreso + '</td> \n\
    <td><input type="hidden" name="break' + cn + '" value="' + h_break + '"/>' + h_break + '</td> \n\
    <td><input type="hidden" name="bkbreak' + cn + '" value="' + h_backbreak + '"/>' + h_backbreak + '</td> \n\
    <td><input type="hidden" name="salida' + cn + '" value="' + h_salida + '"/>' + h_salida + '</td> \n\
        <td><a class="btn-sm btn-round btn-danger" onclick="eliminarHora(' + cn + ');"><i class="fa fa-minus"></i></a></td> \n\
</tr>';
        $('#example12').append(cad);
        $('#modalNuevaHora').modal('hide');
    }

    /*$("#dia").change(function () {
     var valDia1 = "" + $("#dia").val() + "";
     var cant_dia = $('#dia option:selected').size();
     var cadena = valDia1,
     separador = ",", // un espacio en blanco
     limite = cant_dia,
     arregloDeSubCadenas = cadena.split(separador, limite);
     document.getElementById('detalleInfoDias').innerHTML = "Cant: " + cant_dia + " Valor: " + valDia1 + " cantArray:" + arregloDeSubCadenas.length;
     arregloDeSubCadenas.forEach(function (valor, indice, array) {
     console.log("En el índice " + indice + " hay este valor: " + valor);
     });
     });*/
    function eliminarHora(id)
    {
        //$("#dia").find("option[value='1']").removeAttr('selected');  
        //$('#dia option:contains("1")').removeAttr('selected');
        //$('#dia option:contains("1")').attr('selected', 'selected');
        $('#' + id).remove();
    }
</script>