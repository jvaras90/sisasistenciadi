<?php
$config = new Config();
//print_r($arrayOperador)
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Gestión de Mantenimiento
            </h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <!--<input class="errorImput1" type="text" id="dni" name="dni" placeholder="DNI" required>-->
                        <h2>Lista de Operadores <small> </small> </h2>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php
                        if (isset($_GET['msj'])) {
                            $mensaje = $_GET['msj'];
                            ?>
                            <div id="mensaje" style="font-size: 20px;">
                                <label class="label label-warning"> <?php
                                    if ($mensaje == 1) {
                                        echo "El agente fue agregado correctamente";
                                    } else {
                                        echo "Hubo un problema al agregar al agente";
                                    }
                                    ?></label>
                            </div>
                        <?php } ?> 
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <a href="#modalNuevoAgente" class="btn btn-success" data-toggle="modal"><i class="fa fa-user"></i> Nuevo Operador</a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <table id="example" class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th>
                                    <input type="checkbox" id="check-all" class="flat">
                                </th>
                                <th class="column-title">Nombres</th>
                                <th class="column-title">Dni</th>
                                <th class="column-title">Area</th>
                                <th class="column-title">Turno</th>
                                <th class="column-title">Estado</th>
                                <th class="column-title">Marcación</th>
                                <th class="column-title">Detalle</th>
                                <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                                </th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($arrayOperador as $key => $value) {
                                ?> 
                                <tr class="even pointer">
                                    <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td>
                                    <td class="">
                                        <?php
                                        echo utf8_decode($value['nombre']) . " " . utf8_decode($value['apepa']) . " " . utf8_decode($value['apema']);
                                        if ($_SESSION['id_nivel'] == 1) {
                                            echo "<b> (id: " . $value['id_operador'] . ")</b><br>";
                                        }
                                        echo "Sede: " . $value['sede'];
                                        ?>
                                    </td>
                                    <td class=""><?php echo $value['dni']; ?></td>
                                    <td class=""><?php echo $value['area']; ?></td>
                                    <td class=""><?php echo $value['turno']; ?></td>
                                    <td class=""><?php echo $value['estado']; ?></td>
                                    <td class=""><?php echo $value['Marcacion']; ?></td>
                                    <td class="last">
                                        <form method="GET" id="exclude_form_<?php echo $value['id_operador']; ?>" action="">
                                            <div class="col-md-12 col-sm-12 col-xs-12"> 
                                                <div class="col-md-6 col-sm-6 col-xs-12"> 
                                                    <a onclick="listarDetOperador(
                                                    <?php echo $value['id_operador']; ?>,
                                                    <?php echo $value['idarea']; ?>,
                                                    <?php echo $value['idturno']; ?>,
                                                    <?php echo $value['idtipo_usuario']; ?>,
                                                    <?php echo $value['idestado']; ?>,
                                                    <?php echo $value['idmarcacion_est']; ?>,
                                                    <?php echo $value['idhorario']; ?>
                                                                )" class="btn-sm btn-round btn-primary"title="Ver detalle" style="width: 100%"><i class="fa fa-pencil"></i></a>
                                                    <br><br>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12"> 
                                                    <?php if ($value['idestado'] == 2) { ?>
                                                        <a class="btn-sm btn-round  btn-success" onclick="habilitar('<?php echo $value['id_operador']; ?>', '<?php echo $value['nombre']; ?>', 'id_operador', 'operador')"title="Activar Operario" style="width: 100%"><i class="fa fa-check"></i></a>   
                                                    <?php } else if ($value['idestado'] == 1) { ?>
                                                        <a class="btn-sm btn-round  btn-danger" onclick="deshabilitar('<?php echo $value['id_operador']; ?>', '<?php echo $value['nombre']; ?>', 'id_operador', 'operador')"title="Desactivar Operario" style="width: 100%"><i class="fa fa-minus"></i></a>   
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>                                            
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ############################################ MODAL NUEVO AGENTE ############################################ -->

<div class="modal fade" id="modalNuevoAgente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="formRegistroOperador" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nuevo Operador    <i style="color: red" id="messageError"></i> </h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>DNI</label>
                            <div class="input-group" id="dniG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
                                <input type="text" id="dni" name="dni" class="form-control" placeholder="DNI" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Nombres</label>
                            <div class="input-group" id="nombreG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                <input type="text"  id="nombre" name="nombre" class="form-control" placeholder="Nombres" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>A. Paterno</label>
                            <div class="input-group" id="apepaG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                <input type="text"  id="apepa" name="apepa" class="form-control" placeholder="Apellido Paterno" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>A. Materno</label>
                            <div class="input-group" id="apemaG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                <input type="text"  id="apema" name="apema" class="form-control" placeholder="Apellido Materno" style="text-transform:uppercase;" onkeyup="javascript:this.value = this.value.toUpperCase();" required>
                            </div>
                        </div>
                    </div>  
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Área</label>
                            <div class="input-group" id="idareaG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                                <div id="divOpe_area"> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Turno</label>
                            <div class="input-group" id="idturnoG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                <div id="divOpe_turno">

                                </div> 
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Horario</label>
                            <div class="input-group" id="idturnoG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
                                <div id="divOpe_horario">

                                </div> 
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Tipo de Usuario</label>
                            <div class="input-group" id="idtipo_usuarioG">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <div id="divOpe_tipo_user">

                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="button" onclick="validarDatos();" id="nuevoAgente" name="nuevoAgente" class="btn btn-primary">Guardar</button>
                    <input type="hidden" name="form" value="nuevoAgente"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
<!-- icheck -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/icheck/icheck.min.js"></script> 
<!-- Datatables -->
<script src="<?php echo $config->getConfig()['urlBase']; ?>js/datatables/js/jquery.dataTables.js"></script> 
<script>

</script>
<script>
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Buscar:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': <?php echo "10"; ?>,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>