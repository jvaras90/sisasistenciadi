<?php
//include_once '../models/MHorario.php';
$config = new Config();
if (isset($_GET['generar'])) {
    $fecha_ini = $_GET['fecDesde'];
    $fecha_fin = $_GET['fecHasta'];
    $cond = "";
    if (isset($_GET['idturno']) && $_GET['idturno'] != '') {
        $cond .= " and o.idturno = " . $_GET['idturno'];
    }
    if (isset($_GET['idsede']) && $_GET['idsede'] != '') {
        $cond .= " and o.idsede = " . $_GET['idsede'];
    }

    $dia_inicio = date("d", strtotime($fecha_ini));
    $dia_fin = date("d", strtotime($fecha_fin));
    
    $mes = date("m", strtotime($fecha_fin));
    //$mes = date('m');
    $año = date('Y');
    if (isset($_GET['id'])) {
        $id = "and o.id_operador in ($id)";
    } else {
        $cond .= " ";
        //$cond = "and o.id_operador = 85 ";
    }
    ?>
    <style>
        #Exportar_a_Excel, #Exportar_a_Excel td {
            border: #000 1px solid;
        }
        table {     
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;    
            /* margin: 45px;     
            //width: 480px; */
            text-align: left;    
        }        
        th {     
            font-size: 13px;     
            font-weight: normal;     
            padding: 8px;     
        }
        td {    
            padding: 8px;     
        }
        tr:hover td { 
            background: #d0dafd; 
            color: black; 
        }
    </style>
    <style type="text/css">
        #global2 {
            height: 500px;
            width: 100%;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
            overflow-x: auto;
            white-space: nowrap;
            padding-bottom: 25px; 
            padding-right: 25px;
        } 
        #global1 {
            height: 500px;
            width: 30%;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
            overflow-x: auto;
            white-space: nowrap;
            padding-bottom: 25px; 
            padding-right: 25px;
        } 
    </style> 
    <div class="col-md-12">
        <!--        <div class="col-md-2" id="global1">
                    <table style="text-align:center;" border="1" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr bgcolor="#3498DB" style="color:#ffffff">
                                <th style='text-align: center;width: 100%'>Operador</th>   
                                
                            </tr>
                            <tr bgcolor="#3498DB" style="color:#ffffff"> 
                                <th  style='text-align: center;width: 100%'>&nbsp;</th>  
                            </tr>
                        </thead>
                        <tbody>
        <?php
        $sql2 = "select   o.* , concat(nombre,' ', apepa,' ',apema) nombres , se.descripcion sede , tu.descripcion turno
from operador o 
inner join sede se on se.idsede = o.idsede
inner join turno tu on tu.idturno = o.idturno
where o.idestado = 1 $cond
order by idsede,idturno, nombre";
        //echo $sql;
        //$reg2 = $cn->consultasLibres($sql2);
        //$object2 = $reg2['object'];
        //for ($x = 0; $x < count($object2); $x++) {
        ?>
                                <tr>
                                    <td><?php //echo utf8_decode($object2[$x]['nombres']);           ?>    </td>
                                </tr>
        
        <?php //}  ?>
                        </tbody>
                    </table>
                </div> -->
        <div style="text-align: center;"><h2><b>Mes de <?php echo $config->getNombreMesBy($mes);?></b></h2></div>
        <div class="col-md-10" id="global2">

            <table style="text-align:center;" border="1" cellspacing="0" cellpadding="0" id="Exportar_a_Excel" >
                <thead> 
                    <tr bgcolor="#3498DB" style="color:#ffffff">
                        <th rowspan="2" style='text-align: center;width: 250px'>Operador</th>
                        <th rowspan="2" style='text-align: center;width: 50px'>Turno</th>
                        <th rowspan="2" style='text-align: center;width: 50px'>Sede</th>
                        <?php
                        $dia_nor = $dia_inicio + 1 - 1;
                        for ($a = $dia_nor; $a <= $dia_fin; $a++) {

                            if ($a < 10) {
                                $dia = "0$a";
                            } else {
                                $dia = $a;
                            }
                            $fecha = "$año-$mes-$dia"; 
                            $nombre = $config->get_nombre_dia($fecha);
                            echo "<th colspan='3' style='text-align: center;'>$nombre $dia </th>";
                        }
                        ?>
                        <th rowspan="2" style='text-align: center;width: 50px'>Total Acumulado</th>
                    </tr>
                    <tr style="color:#ffffff">
                        <?php
                        for ($b = $dia_inicio; $b <= $dia_fin; $b++) {
                            echo "<th style='text-align: center;' bgcolor='#26B99A'> Ing. </th>";
                            echo "<th style='text-align: center;' bgcolor='#E74C3C'> Sal. </th>";
                            echo "<th style='text-align: center;' bgcolor='#455C73'> Tot. </th>";
                        }
                        ?>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $sql = "select   o.* , concat(nombre,' ', apepa,' ',apema) nombres , se.descripcion sede , tu.descripcion turno
from operador o 
inner join sede se on se.idsede = o.idsede
inner join turno tu on tu.idturno = o.idturno
where o.idestado = 1 $cond
order by idsede,idturno, nombre";
                    //echo $sql;
                    $reg = $cn->consultasLibres($sql);
                    $object = $reg['object'];
                    for ($x = 0; $x < count($object); $x++) {
                        ?>
                        <tr title="<?php echo utf8_decode($object[$x]['nombres']); ?>">
                            <td style="width: 200px;"><?php echo utf8_decode($object[$x]['nombres']); ?></td>
                            <td style="width: 200px;"><?php echo $object[$x]['turno']; ?></td>
                            <td style="width: 200px;"><?php echo $object[$x]['sede']; ?></td>
                            <?php
                            $total_G = 0;
                            $dia_nor = $dia_inicio + 1 - 1;
                            for ($a = $dia_nor; $a <= $dia_fin; $a++) {
                                $query2 = "select *, 
TIMEDIFF(TIMEDIFF( end_day, begin_day),TIMEDIFF( backbreak, break)) total_horas,
TIME_TO_SEC(TIMEDIFF(TIMEDIFF( end_day, begin_day),TIMEDIFF( backbreak, break))) total_horas_segundos
                        from marcacion 
                        where idoperador= " . $object[$x]['id_operador'] . " and 
                        month(fecha_marcada)=$mes and 
                        day(fecha_marcada)= $a ;";
                                $reg2 = $cn->consultasLibres($query2);
                                $object2 = $reg2['object'];
                                //print_r($object2); //exit();
                                if (count($object2) > 0) {
                                    if ($object2[0]['total_horas'] < '00:00:00') {
                                        if ($object2[0]['total_horas'] == '-01:00:00') {
                                            $total = '<b style="color:red;">Ausente</b>';
                                        } else {
                                            $total = $object2[0]['total_horas'];
                                        }
                                    } else {
                                        $total = $object2[0]['total_horas'];
                                        $total_seg = $object2[0]['total_horas_segundos'];
                                        $total_G = $total_G + $total_seg;
                                    }
                                    echo "<td  bgcolor='#F0F0F0' title='Ingreso $a/$mes de " . utf8_decode($object[$x]['nombres']) . "'>" . $object2[0]['begin_dayf'] . "</td>";
                                    echo "<td  bgcolor='#F0F0F0' title='Salida  $a/$mes de " . utf8_decode($object[$x]['nombres']) . "'>" . $object2[0]['end_dayf'] . "</td>";
                                    echo "<td  bgcolor='#F0F0F0' title='Total Horas  $a/$mes de " . utf8_decode($object[$x]['nombres']) . "'>" . $total . "</td>";
                                } else {
                                    echo "<td  bgcolor='#F0F0F0'>00:00:00</td>";
                                    echo "<td  bgcolor='#F0F0F0'>00:00:00</td>";
                                    echo "<td  bgcolor='#F0F0F0'>0</td>";
                                }
                            }
                            ?>
                            <td style="width: 200px;"><?php echo $config->getSegundoHoras($total_G); ?></td>
                        </tr> 
                    <?php } ?>
                </tbody> 
            </table>
        </div>
    </div>
    <?php
}
?>