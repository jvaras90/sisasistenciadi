<?php
//include_once '../models/MHorario.php';
$config = new Config();
if (isset($_GET['generar'])) {
    $fecha_ini = $_GET['fecDesde'];
    $fecha_fin = $_GET['fecHasta'];
    $cond = "";
    if (isset($_GET['idturno']) && $_GET['idturno'] != '') {
        $cond .= " and o.idturno = " . $_GET['idturno'];
    }
    if (isset($_GET['idsede']) && $_GET['idsede'] != '') {
        $cond .= " and o.idsede = " . $_GET['idsede'];
    }

    $dia_inicio = date("d", strtotime($fecha_ini));
    $dia_fin = date("d", strtotime($fecha_fin));

    $mes = date("m", strtotime($fecha_fin));
    //$mes = date('m');
    $año = date('Y');
    if (isset($_GET['id'])) {
        $id = "and o.id_operador in ($id)";
    } else {
        $cond .= " ";
        //$cond = "and o.id_operador = 85 ";
    }
    ?>
    <table id="example" class="table table-striped responsive-utilities jambo_table bulk_action">
        <thead>
            <tr class="headings">
                <th>
                    <input type="checkbox" id="check-all" class="flat">
                </th> 
                <th class="column-title">Fecha</th>
                <th class="column-title">Ingreso</th>
                <th class="column-title">Break</th>
                <th class="column-title">Regreso B.</th>
                <th class="column-title">Salida</th>
                <th class="column-title">Puntualidad</th>                
                <th class="column-title" title="Minutos contables de tardanza para el cálculo de Horas Trabajadas">Tardanza</th>
                <th class="column-title" title="Minutos contables de break para el cálculo de Horas Trabajadas">Break</th>
                <th class="column-title">H. Trabajadas</th>
                <?php if ($_SESSION['idarea'] == '2') { ?>
                    <th class="column-title">Detalle</th>
                <?php } ?>
    <!--<th class="column-title no-link last"><span class="nobr">Accion</span>-->
                </th>
                <th class="bulk-actions" colspan="10">
                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($arrayRegistros as $key => $value) {
                ?> 
                <tr class="even pointer" style="text-align: center">
                    <td class="a-center "><input type="checkbox" class="flat" name="table_records" ></td> 
                    <td class="">
                        <?php
                        //echo $value['dia'];
                        echo "<b style='text-align: center;'>" . $config->getConfig()['nomDias'][$value['dia']] . " </b><br>";
                        echo $value['fecha_marcada'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<b> (id: " . $value['idmarcacion'] . ")</b>";
                        }
                        ?>
                    </td>
                    <td class=""><?php
                        echo $value['begin_dayf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['begin_day'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class=""><?php
                        echo $value['breakf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['break'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class=""><?php
                        echo $value['backbreakf'];
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . $value['backbreak'] . "</span>";
                        }
                        ?>
                    </td>
                    <td class="" <?php
                    if ($value['fecha_marcada'] < $config->getFechaCortaActual() && $value['end_dayf'] == "00:00:00" && $value['begin_day'] != '00:00:00') {
                        echo 'style="background-color: #ff8c8c;color:red;" title="Olvidó marcar su salida, esto probablemente se vea afectado en el resultado de Horas Trabajadas. Debe enviar un correo a las personas encargadas para modificar el registro."';
                    }
                    ?> ><?php
                            echo $value['end_dayf'];

                            if ($_SESSION['id_nivel'] == 1) {
                                echo "<br><span style='color:blue'>" . $value['end_day'] . "</span>";
                            }
                            ?>
                    </td>
                    <td class="" title="Referencial a la hora de llegada del personal">
                        <?php
                        //echo $oHorarios->getHorasSegundo($value['tardanzaf']);
                        if ($value['begin_day'] == "00:00:00" && $value['end_day'] == "00:00:00" && $value['break'] == "00:00:00" && $value['backbreak'] == "00:00:00") {
                            echo "<span style='color:red'><b>Ausente</b></span>";
                        } else {
                            if ($value['tardanzaf'] == '00:00:00' || $value['tardanzaf'] < '00:00:00') {
                                echo " Temprano";
                            } else {
                                echo "<span style='color:red'>Tarde</span>";
                            }
                        }
                        ?>
                    </td>                    
                    <td class="" title="Rango de Ingreso Marcado en el sistema
                        Ingreso menor a 8:14       -> Marcación en Sistema 8:00am
                        Ingreso entre 8:15 a 8:44 -> Marcación en Sistema 8:30am
                        Ingreso entre 8:45 a 9:14 -> Marcación en Sistema 9:00am
                        Ingreso después de 9:15  -> Marcación en tiempo real"><?php
                            if ($value['tardanzaf'] == '00:00:00' || $value['tardanzaf'] < '00:00:00') {
                                echo "00:00:00";
                            } else {
                                echo ($value['tardanzaf']);
                            }
                            if ($_SESSION['id_nivel'] == 1) {
                                echo "<br><span style='color:blue'>" . ($value['tardanza']) . "</span>";
                            }
                            ?>
                    </td>
                    <td class=""><?php
                        echo ($value['almuerzo']);
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . ($value['almuerzo']) . "</span>";
                        }
                        ?>
                    </td>
                    <td class="" ><!--Horas Trabajadas-->
                        <?php
                        if ($value['horas_trabajo'] < "00:00:00") {
                            echo "00:00:00";
                        } else {
                            echo $value['horas_trabajo'];
                        }
                        if ($_SESSION['id_nivel'] == 1) {
                            echo "<br><span style='color:blue'>" . ($value['asistencia']) . "</span>";
                        }
                        ?>
                    </td>
                    <?php if ($_SESSION['idarea'] == '2') { ?>
                        <td class=""> 
                            <div class="col-md-12 col-sm-12 col-xs-12"> 

                                <div class="col-md-6 col-sm-6 col-xs-12"> 
                                    <a href="#modalCambiarEstado" data-toggle="modal" class="btn-sm btn-round  btn-danger" onclick="mostrar('<?php echo $value['idmarcacion']; ?>')"title="Modificar Horarios"><i class="fa fa-pencil"></i></a>                                         
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr > 
                <td class=""><?php echo ""; ?></td>
                <td class=""><?php echo ""; ?></td>
                <td class=""><?php echo ""; ?></td>
                <td class=""><?php echo ""; ?></td>
                <td class=""><?php echo ""; ?></td>
                <td class=""><?php echo ""; ?></td>
                <td class=""></td>
                <td class="" title="Tiempo Total de Tardanzas Acumuladas"><b><?php
                        $tarde = "";
                        for ($i = 0; $i < count($arrayRegistros); $i++) {
                            if ($arrayRegistros[$i]['tard_seg'] > 0) {
                                $tarde += $arrayRegistros[$i]['tard_seg'];
                            }
                        }
                        $oHor = new MHorario();
                        $tardeH = $oHor->getSegundoHoras($tarde);
                        echo " $tardeH<br>";
                        ?></b></td>
                <td class="" title="Tiempo Total de Break Acumulados"><b><?php echo ($value['total_almuerzo']); ?></b></td>
                <td class="" title="Total de Horas Trabajadas"><b><?php echo ($value['total_segundos_trabajo']); ?></b></td>
                <?php if ($_SESSION['idarea'] == '2') { ?>
                    <td class="last"><?php echo ""; ?></td>
                <?php } ?>
            </tr>
        </tbody>

    </table>
<?php
}
