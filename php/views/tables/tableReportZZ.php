<table style="text-align:center;padding: 5px;margin: auto;" border="1" cellspacing="0" cellpadding="0" id="Exportar_a_Excel" >
    <thead>
        <tr bgcolor="#3498DB" style="color:#ffffff">
            <th style='text-align: center;' scope="rowgroup" rowspan="1">Operador</th>
            <th style='text-align: center;' scope="rowgroup" rowspan="1">Turno</th>
            <th style='text-align: center;' scope="rowgroup" rowspan="1">Sede</th>
            <th style='text-align: center;' scope="rowgroup" rowspan="1">Detalle</th>
                <?php
                for ($a = $dia_inicio; $a <= $dia_fin; $a++) {
                    if ($a < 10) {
                        $dia = "0$a";
                    } else {
                        $dia = $a;
                    }
                    $nombre = get_nombre_dia("$año-$mes-$dia");
                    echo "<th style='text-align: center;' scope='rowgroup' rowspan='1'>$nombre $dia</th>";
                }
                ?> 
        </tr>  
    </thead>
    <tbody>
        <?php
        $sql = "select   o.* , concat(nombre,' ', apepa,' ',apema) nombres , se.descripcion sede , tu.descripcion turno
from operador o 
inner join sede se on se.idsede = o.idsede
inner join turno tu on tu.idturno = o.idturno
where o.idestado = 1
order by idsede,idturno, nombre";
        $reg = $cn->consultasLibres($sql);
        $object = $reg['object'];
        for ($x = 0; $x < count($object); $x++) {
            ?>
            <tr>
                <td scope="rowgroup" rowspan="4"><?php echo utf8_decode($object[$x]['nombres']); ?></td>
                <td scope="rowgroup" rowspan="4"><?php echo $object[$x]['turno']; ?></td>
                <td scope="rowgroup" rowspan="4"><?php echo $object[$x]['sede']; ?>     
            </tr>   
            <tr>
                <th scope="row">Ingreso</th> 
                <?php
                for ($a = $dia_inicio; $a <= $dia_fin; $a++) {
                    $query1 = "select begin_day
                        from marcacion 
                        where idoperador= " . $object[$x]['id_operador'] . " and 
                        month(fecha_marcada)=9 and 
                        day(fecha_marcada)= $a ;";
                    $reg1 = $cn->consultasLibres($query1);
                    $object1 = $reg1['object'];
                    //print_r($object2); //exit();
                    if (count($object1) > 0) {
                        echo "<td>" . $object1[0]['begin_day'] . "</td>";
                    } else {
                        echo "<td> </td>";
                    }
                }
                ?> 
            </tr>
            <tr>
                <th scope="row">Salida</th> 
                <?php
                for ($a = $dia_inicio; $a <= $dia_fin; $a++) {
                    $query2 = "select end_day
                        from marcacion 
                        where idoperador= " . $object[$x]['id_operador'] . " and 
                        month(fecha_marcada)=9 and 
                        day(fecha_marcada)= $a ;";
                    $reg2 = $cn->consultasLibres($query2);
                    $object2 = $reg2['object'];

                    if (count($object2) > 0) {
                        echo "<td>" . $object2[0]['end_day'] . "</td>";
                    } else {
                        echo "<td></td>";
                    }
                }
                ?> 
            </tr>
            <tr>
                <th scope="row">Total</th> 
                <?php
                for ($a = $dia_inicio; $a <= $dia_fin; $a++) {
                    $query3 = "select TIMEDIFF( 
                        SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( end_day, begin_day)))),
                        SEC_TO_TIME(3600) ) total_horas
                        from marcacion 
                        where idoperador= " . $object[$x]['id_operador'] . " and 
                        month(fecha_marcada)=9 and 
                        day(fecha_marcada)= $a ;";
                    $reg3 = $cn->consultasLibres($query3);
                    $object3 = $reg3['object'];
                    $total_horas = $object3[0]['total_horas'];
                    if ($total_horas == "") {
                        $tot = "";
                    } else if ($total_horas == '-01:00:00') {
                        $tot = "<b style='color:red;'>Ausente</b>";
                    } else {
                        $tot = $total_horas;
                    }
                    echo "<td>$tot</td>";
                }
                ?> 
            </tr>
        <?php } ?>
    </tbody>
</table>