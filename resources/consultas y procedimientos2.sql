select * from operador;
update operador set idmarcacion_est = 3 where idestado =1 ;

select * from operador where id_operador=85
;
select * from marcacion where fecha_marcada = '2017-09-00'
;

select * from operador;

select * from marcacion where idmarcacion=92;

select * from marcacion where fecha_marcada = '2017-09-04';

select m.idmarcacion, m.idoperador, concat(nombre, ' ', apepa, ' ', apema ) nombres, m.fecha_marcada, 
m.begin_day, m.break, m.backbreak, m.end_day,o.idtipo_usuario, m.begin_dayf, m.breakf, m.backbreakf, 
m.end_dayf, t.h_ingreso, t.h_break, t.h_backbreak, t.h_salida, 
TIMEDIFF( m.begin_day, t.h_ingreso) tardanza, 
TIME_TO_SEC(TIMEDIFF( m.begin_day, t.h_ingreso)) tard_seg,
TIMEDIFF( m.begin_dayf, t.h_ingreso) tardanzaf, 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia, 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo, 
timediff(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))), 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break))))) horas_trabajo, 
m.idturno, t.descripcion turno, ip_marc_ing, ip_marc_bre, ip_marc_bbk, ip_marc_end, 
(select SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( end_day, begin_day)))) total from marcacion where idoperador = 85 and month(fecha_marcada) = 09 ) total_asistencia, 
(select SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( backbreak, break)))) total from marcacion where idoperador = 85 and month(fecha_marcada) = 09 ) total_almuerzo, 
(select SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( n.begin_day, ty.h_ingreso)))) total from marcacion n inner join turno ty on ty.idturno = n.idturno where m.idmarcacion = n.idmarcacion and idoperador = 85 and month(fecha_marcada) = 09 ) total_tardanza, 
timediff(
(select SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( end_day, begin_day)))) total from marcacion where idoperador = 85 and month(fecha_marcada) = 09 ), 
(select SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( backbreak, break)))) total from marcacion where idoperador = 85 and month(fecha_marcada) = 09 ) 
) total_segundos_trabajo 
from marcacion m 
inner join operador o on o.id_operador = m.idoperador 
inner join turno t on m.idturno = t.idturno 
where m.idoperador = 85 
and month(fecha_marcada) = 09 
group by day(fecha_marcada) 
order by fecha_marcada desc;


select n.begin_day , ty.h_ingreso,
TIMEDIFF( n.begin_day, ty.h_ingreso) tardanza, 
TIMEDIFF( n.begin_dayf, ty.h_ingreso) tardanzaf, 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( n.begin_day, ty.h_ingreso)))) total 
from marcacion n 
inner join turno ty on ty.idturno = n.idturno
where idoperador = 85 and month(fecha_marcada) = 09;



select m.idmarcacion, m.idoperador, o.dni, o.idtipo_usuario,
(select n.begin_day from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) begin_day,
(select n.break from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) break,
(select n.backbreak from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) backbreak,
(select n.end_day from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) end_day,
(select n.begin_dayf from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) begin_dayf,
(select n.breakf from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) breakf,
(select n.backbreakf from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) backbreakf,
(select n.end_dayf from marcacion n where n.idmarcacion = m.idmarcacion and day(fecha_marcada) = day(now())) end_dayf,
 concat(nombre, ' ', apepa, ' ', apema ) nombres, o.idturno, o.idarea,
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_day, t.h_ingreso)))) tardanza,
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia,
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo,
 TIMEDIFF( SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))),
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) ) as total_horas
from marcacion m
inner join operador o on o.id_operador = m.idoperador
inner join turno t on m.idturno = t.idturno
where month(fecha_marcada) = month(now()) and o.idestado = 1
group by m.idoperador
order by nombre ;

/*REPORTE DE ASISTENCIA DETALLADO POR MES*/
select m.idmarcacion, m.idoperador, concat(nombre, ' ', apepa, ' ', apema ) nombres, m.fecha_marcada, 
m.begin_day, m.break, m.backbreak, m.end_day,o.idtipo_usuario, m.begin_dayf, m.breakf, m.backbreakf, 
m.end_dayf, t.h_ingreso, t.h_break, t.h_backbreak, t.h_salida
from marcacion m
inner join operador o on o.id_operador = m.idoperador
inner join turno t on m.idturno = t.idturno 
where month(fecha_marcada) = month(now()) and o.idestado = 1
order by nombre 
;

/*REPORTE DE ASISTENCIA DETALLADO POR dia*/
select m.idmarcacion, m.idoperador, concat(nombre, ' ', apepa, ' ', apema ) nombres, m.fecha_marcada, 
m.begin_day, m.break, m.backbreak, m.end_day,o.idtipo_usuario, m.begin_dayf, m.breakf, m.backbreakf, 
TIMEDIFF( m.begin_day, t.h_ingreso) tardanza, 
TIME_TO_SEC(TIMEDIFF( m.begin_day, t.h_ingreso)) tard_seg,
TIMEDIFF( m.begin_dayf, t.h_ingreso) tardanzaf, 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia, 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo, 
timediff(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))), 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break))))) horas_trabajo, 
m.end_dayf, t.h_ingreso, t.h_break, t.h_backbreak, t.h_salida, o.idmarcacion_est , e.descripcion estMarcacion
from marcacion m
inner join operador o on o.id_operador = m.idoperador
inner join turno t on m.idturno = t.idturno 
inner join estado e on e.idestado = o.idmarcacion_est
where month(fecha_marcada) = month(now()) and day(fecha_marcada) = day(now()) and o.idestado = 1
group by m.idoperador
order by o.idsede,o.idturno,nombre 
;

select * 
from operador o 
left join marcacion m on m.idoperador = o.id_operador
where o.idestado = 1
;
select * from operador;
select * from horario_marc;
select * from horario;

select * from estado;
select * from operador where idsede=1;

select * from marcacion where idoperador in (select id_operador from operador where idsede=2) order by fecha_marcada , idoperador;

select * from operador where idsede = 2;
select * from horario;
select * from horario_marc;
select * from turno;

select * from horario_marc where idhorario = 1 and dia = 1;

select id_operador,o.nombre,apepa,apema,f_ing , f_egr , o.dni , o.idestado,
            foto, o.idturno , tur.descripcion turno,o.idarea , a.descripcion area,
            o.idtipo_usuario , tu.descripcion tipo_user,o.lastupdate,idmarcacion_est,
			o.idsede,se.descripcion sede ,o.idhorario , ho.descripcion horario
            from operador o 
            inner join turno tur on tur.idturno = o.idturno
            inner join area a on a.idarea = o.idarea
            inner join tipo_usuario tu on tu.idtipo_usuario = o.idtipo_usuario
            inner join sede se on se.idsede  = o.idsede
            inner join horario ho on ho.idhorario = o.idhorario
            where o.dni = '46241427';
select * from turno;

select * from operador where idturno != 1;


select  o.id_operador, o.nombre , apepa , apema , f_ing , f_egr , dni , telefono,email,direccion,
            o.foto, o.idturno , t.descripcion turno, o.idarea, a.descripcion area , 
            o.idtipo_usuario, tu.descripcion tipo_usuario , o.idestado , e.descripcion estado , 
            o.idmarcacion_est , em.descripcion Marcacion,            
            hm.h_ingreso,hm.h_break,hm.h_backbreak,hm.h_salida
            from operador o
            inner join turno t on t.idturno = o.idturno
            inner join area a on a.idarea = o.idarea 
            inner join tipo_usuario tu on tu.idtipo_usuario = o.idtipo_usuario
            inner join estado e on e.idestado = o.idestado
            inner join estado em on em.idestado = o.idmarcacion_est
            inner join sede se on se.idsede = o.idsede
            inner join horario ho on ho.idhorario = o.idhorario
            inner join horario_marc hm on ho.idhorario = hm.idhorario
            where hm.dia = 1 and o.id_operador = 85;
            
select * from operador where idturno= 2;     

select * from marcacion where fecha_marcada = '2017-09-05';       

select * from horario;
select * from operador where idturno=2;


select m.idmarcacion, m.idoperador, o.dni, o.idtipo_usuario,
 concat(nombre, ' ', apepa, ' ', apema ) nombres, o.idturno, o.idarea,
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_day, hm.h_ingreso)))) tardanza,
 
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia,
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo,
 TIMEDIFF( 
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))),
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) ) as total_horas
from marcacion m
inner join operador o on o.id_operador = m.idoperador
inner join turno t on m.idturno = t.idturno
inner join sede se on se.idsede = o.idsede
inner join horario ho on ho.idhorario = o.idhorario
inner join horario_marc hm on ho.idhorario = hm.idhorario
where month(fecha_marcada) = month(now()) and o.idestado = 1 and idoperador = 17
group by m.idoperador
order by nombre ;

select m.idmarcacion,
m.dia, m.idoperador, m.fecha_marcada, 
/*m.begin_day , m.break , m.backbreak , m.end_day ,       
hm.h_ingreso,hm.h_break,hm.h_backbreak,hm.h_salida,*/
m.idturno as idhorarioM ,o.dni, o.idtipo_usuario,
concat(nombre, ' ', apepa, ' ', apema ) nombres, o.idturno, o.idarea , o.idsede , 
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_day, hm.h_ingreso)))) tardanza,
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.begin_dayf, hm.h_ingreso)))) tardanzaf,
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))) asistencia,
SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) almuerzo,
TIMEDIFF( 
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.end_day, m.begin_day)))),
 SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF( m.backbreak, m.break)))) 
) as total_horas
from marcacion m 
inner join operador o on o.id_operador = m.idoperador
inner join horario ho on ho.idhorario = o.idhorario
inner join horario_marc hm on ho.idhorario = hm.idhorario and m.dia = hm.dia
where  month(fecha_marcada) = month(now())  and o.idestado = 1 and m.idestado != 3
group by m.idoperador
order by o.idestado,o.idsede,o.idturno,nombre;

select * from marcacion;

update marcacion set idestado = 3 where idmarcacion in (314,316,317,404);
select * from horario;

update marcacion set idestado= 3 where idoperador=86;