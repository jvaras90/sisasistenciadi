CREATE DATABASE  IF NOT EXISTS `dataimag_dbcontrol1` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `dataimag_dbcontrol1`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 198.49.74.82    Database: dataimag_dbcontrol1
-- ------------------------------------------------------
-- Server version	5.5.56-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `idarea` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idarea`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id_cliente` int(11) DEFAULT NULL,
  `nombre_cliente` varchar(50) DEFAULT NULL,
  UNIQUE KEY `id_cliente` (`id_cliente`),
  KEY `Índice 1` (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `control`
--

DROP TABLE IF EXISTS `control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control` (
  `id_control` int(11) NOT NULL AUTO_INCREMENT,
  `id_operador` int(11) DEFAULT '0',
  `id_proced` int(11) DEFAULT '0',
  `balda` varchar(50) DEFAULT '0',
  `lote` varchar(11) DEFAULT '0',
  `f_ini` date DEFAULT NULL,
  `f_fin` date DEFAULT NULL,
  `h_ini` time DEFAULT NULL,
  `h_fin` time DEFAULT NULL,
  `cant_imag` int(11) DEFAULT '0',
  `f_registro` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `observa` varchar(50) DEFAULT '0',
  `id_cliente` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_control`),
  UNIQUE KEY `id_control_2` (`id_control`),
  KEY `id_control` (`id_control`)
) ENGINE=InnoDB AUTO_INCREMENT=52660 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `idhorario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `idturno` int(11) NOT NULL DEFAULT '1',
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idhorario`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `horario_marc`
--

DROP TABLE IF EXISTS `horario_marc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario_marc` (
  `idhorario_marc` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '00:00:00',
  `h_ingreso` time NOT NULL DEFAULT '00:00:00',
  `h_break` time NOT NULL DEFAULT '00:00:00',
  `h_backbreak` time NOT NULL DEFAULT '00:00:00',
  `h_salida` time NOT NULL DEFAULT '00:00:00',
  `idhorario` int(11) NOT NULL DEFAULT '1',
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idhorario_marc`),
  KEY `fk_horario_turno_idx` (`idhorario`),
  KEY `fk_horario_estado_idx` (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `marcacion`
--

DROP TABLE IF EXISTS `marcacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcacion` (
  `idmarcacion` int(11) NOT NULL AUTO_INCREMENT,
  `dia` int(11) NOT NULL DEFAULT '1',
  `idoperador` int(11) NOT NULL,
  `fecha_marcada` date NOT NULL,
  `begin_day` time NOT NULL DEFAULT '00:00:00',
  `break` time NOT NULL DEFAULT '00:00:00',
  `backbreak` time NOT NULL DEFAULT '00:00:00',
  `end_day` time NOT NULL DEFAULT '00:00:00',
  `begin_dayf` time NOT NULL DEFAULT '00:00:00',
  `breakf` time NOT NULL DEFAULT '00:00:00',
  `backbreakf` time NOT NULL DEFAULT '00:00:00',
  `end_dayf` time NOT NULL DEFAULT '00:00:00',
  `idturno` int(11) NOT NULL,
  `observacion1` varchar(512) DEFAULT NULL,
  `observacion2` varchar(512) DEFAULT NULL,
  `ip_marc_ing` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `ip_marc_bre` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `ip_marc_bbk` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `ip_marc_end` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `hora_falsa` time NOT NULL DEFAULT '00:00:00',
  `idestado` int(11) NOT NULL DEFAULT '1',
  `user_pc` varchar(245) NOT NULL DEFAULT '',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmarcacion`),
  KEY `fk_marcacion_operador_idx` (`idoperador`),
  KEY `fk_marcacion_turno_idx` (`idturno`),
  KEY `fk_marcacion_estado_idx` (`idestado`),
  KEY `indx_fecha_marcado` (`fecha_marcada`),
  CONSTRAINT `fk_marcacion_estado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_marcacion_operador` FOREIGN KEY (`idoperador`) REFERENCES `operador` (`id_operador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_marcacion_turno` FOREIGN KEY (`idturno`) REFERENCES `turno` (`idturno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operador`
--

DROP TABLE IF EXISTS `operador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operador` (
  `id_operador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `apepa` varchar(100) DEFAULT NULL,
  `apema` varchar(100) DEFAULT NULL,
  `f_ing` date NOT NULL DEFAULT '0000-00-00',
  `f_egr` date NOT NULL DEFAULT '0000-00-00',
  `dni` varchar(15) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(145) DEFAULT NULL,
  `direccion` varchar(245) DEFAULT NULL,
  `foto` varchar(145) NOT NULL DEFAULT 'user.png',
  `idturno` int(11) NOT NULL DEFAULT '1',
  `idarea` int(11) NOT NULL DEFAULT '3',
  `idtipo_usuario` int(11) NOT NULL DEFAULT '3',
  `idsede` int(11) NOT NULL DEFAULT '1',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `idmarcacion_est` int(11) NOT NULL DEFAULT '3',
  `idhorario` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_operador`),
  UNIQUE KEY `dni_UNIQUE` (`dni`),
  KEY `fk_operador_turno_idx` (`idturno`),
  KEY `fk_operador_area_idx` (`idarea`),
  KEY `fk_operador_tipoUser_idx` (`idtipo_usuario`),
  KEY `idestado_idx` (`idestado`),
  KEY `idmarc_est_idx` (`idmarcacion_est`),
  KEY `fk_operador_sede_idx` (`idsede`),
  KEY `fk_operador_horario_idx` (`idhorario`),
  CONSTRAINT `fk_operador_area` FOREIGN KEY (`idarea`) REFERENCES `area` (`idarea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operador_tipoUser` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_operador_turno` FOREIGN KEY (`idturno`) REFERENCES `turno` (`idturno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idestado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idmarc_est` FOREIGN KEY (`idmarcacion_est`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `procedimientos`
--

DROP TABLE IF EXISTS `procedimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedimientos` (
  `id_proced` int(11) NOT NULL AUTO_INCREMENT,
  `des_proced` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_proced`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reg_mod_marcacion`
--

DROP TABLE IF EXISTS `reg_mod_marcacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_mod_marcacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_day` time NOT NULL,
  `break` time NOT NULL,
  `backbreak` time NOT NULL,
  `end_day` time NOT NULL,
  `begin_dayf` time NOT NULL,
  `breakf` time NOT NULL,
  `backbreakf` time NOT NULL,
  `end_dayf` time NOT NULL,
  `h_ingO` time NOT NULL,
  `h_brkO` time NOT NULL,
  `h_bbkO` time NOT NULL,
  `h_egrO` time NOT NULL,
  `h_ingFO` time NOT NULL,
  `h_brkFO` time NOT NULL,
  `h_bbkFO` time NOT NULL,
  `h_egrFO` time NOT NULL,
  `idmarcacion` int(11) NOT NULL,
  `motivo` text NOT NULL,
  `fecha_mod` date NOT NULL,
  `hora_mod` time NOT NULL,
  `idoperador` int(11) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_reg_mod_marcacion_idmarcacion_idx` (`idmarcacion`),
  KEY `fk_reg_mod_marcacion_operador_idx` (`idoperador`),
  KEY `fk_reg_mod_marcacion_estado_idx` (`idestado`),
  CONSTRAINT `fk_reg_mod_marcacion_estado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reg_mod_marcacion_idmarcacion` FOREIGN KEY (`idmarcacion`) REFERENCES `marcacion` (`idmarcacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reg_mod_marcacion_operador` FOREIGN KEY (`idoperador`) REFERENCES `operador` (`id_operador`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sede`
--

DROP TABLE IF EXISTS `sede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sede` (
  `idsede` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) COLLATE utf8_unicode_ci NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idsede`),
  KEY `fk_sede_estado_idx` (`idestado`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_operador`
--

DROP TABLE IF EXISTS `tipo_operador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_operador` (
  `Id_Tipo` int(11) NOT NULL AUTO_INCREMENT,
  `Desc_Tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_Tipo`),
  KEY `Id_Tipo` (`Id_Tipo`),
  KEY `Id_Tipo_2` (`Id_Tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) NOT NULL,
  `idestado` int(11) NOT NULL DEFAULT '1',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `idturno` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(145) NOT NULL,
  `h_ingreso` time NOT NULL DEFAULT '00:00:00',
  `h_break` time NOT NULL DEFAULT '00:00:00',
  `h_backbreak` time NOT NULL DEFAULT '00:00:00',
  `h_salida` time NOT NULL DEFAULT '00:00:00',
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idestado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idturno`),
  KEY `fk_turno_estado_idx` (`idestado`),
  CONSTRAINT `fk_turno_estado` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(10) NOT NULL,
  `passwoord` varchar(50) NOT NULL,
  `des_user` varchar(150) NOT NULL,
  `Rol` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-13 15:04:39
